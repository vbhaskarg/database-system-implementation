#include <algorithm>
#include <cstring>
#include "Node.h"

Node :: Node() {
	outNode = NULL;
	outPipe = new Pipe(PIPE_SIZE);
}

Node :: ~Node() {
	delete outPipe;
}

RelNode :: RelNode(char *tableName, char *alias) {
	char fileName[50];
	Attribute *atts;

	sprintf(fileName, "%s.bin", tableName);

	this->tableName = strdup(tableName);
	this->alias = strdup(alias);

	outSchema = new Schema(db_catalog_path, tableName);
	atts = outSchema->GetAtts();
	for (int i = 0; i < outSchema->GetNumAtts(); i++) {
		char newAttName[50];
		sprintf(newAttName, "%s.%s", alias, atts[i].name);
		delete atts[i].name;
		atts[i].name = strdup(newAttName);
	}

	dbFile.Open(fileName);
}

RelNode :: ~RelNode() {
	dbFile.Close();

	delete outSchema;
	free(tableName);
	free(alias);
}

void RelNode :: CreateCNF(AndList *relAndList) {
	cnf.GrowFromParseTree(relAndList, outSchema, literal);
}

void RelNode :: Run() {
	relSelect.Use_n_Pages (NUM_PAGES);
	relSelect.Run (dbFile, *outPipe, cnf, literal);
}

void RelNode :: Print() {
	cout << "*** SelectFile opertaion ***" << endl;
	cout << "Input " << tableName << ".bin" << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << "Select CNF:" << endl;
	cnf.Print();
	cout << endl;
}

JoinNode :: JoinNode(Node *leftInNode, Node *rightInNode, AndList *parseTree) {
	this->leftInNode = leftInNode;
	this->rightInNode = rightInNode;
	leftInPipe = leftInNode->outPipe;
	rightInPipe = rightInNode->outPipe;

	int numLeftAtts = leftInNode->outSchema->GetNumAtts();
	int numRightAtts = rightInNode->outSchema->GetNumAtts();
	int numAtts = numLeftAtts +	numRightAtts;
	Attribute *atts = new Attribute[numAtts];

	Attribute *leftAtts = leftInNode->outSchema->GetAtts();
	for (int i = 0; i < numLeftAtts; i++) {
		atts[i].name = leftAtts[i].name;
		atts[i].myType = leftAtts[i].myType;
	}

	Attribute *rightAtts = rightInNode->outSchema->GetAtts();
	for (int i = numLeftAtts; i < numAtts; i++) {
		atts[i].name = rightAtts[i - numLeftAtts].name;
		atts[i].myType = rightAtts[i - numLeftAtts].myType;
	}

	outSchema = new Schema("join_schema", numAtts, atts);
	delete[] atts;

	cnf.GrowFromParseTree(parseTree, leftInNode->outSchema,
		rightInNode->outSchema, literal);

	relJoin.Run(*leftInPipe, *rightInPipe, *outPipe, cnf, literal);
}

JoinNode :: ~JoinNode() {
	delete outSchema;
	delete leftInNode;
	delete rightInNode;
}

void JoinNode :: Print() {
	leftInNode->Print();
	rightInNode->Print();
	cout << "*** Join opertaion ***" << endl;
	cout << "Input Pipe ID " << leftInNode->outPipeId << endl;
	cout << "Input Pipe ID " << rightInNode->outPipeId << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << "Join CNF:" << endl;
	cnf.Print();
	cout << endl;
}

SumNode :: SumNode(Node *inNode, FuncOperator *finalFunction) {
	Attribute att;
	char attName[5] = "sum";

	this->inNode = inNode;
	inPipe = inNode->outPipe;

	func.GrowFromParseTree (finalFunction, *inNode->outSchema);

	att.name = attName;
	att.myType = func.ReturnType();
	outSchema = new Schema("sum_schema", 1, &att);

	sum.Run(*inPipe, *outPipe, func);
}

SumNode :: ~SumNode() {
	delete outSchema;
	delete inNode;
}

void SumNode :: Print() {
	inNode->Print();
	cout << "*** Aggregate opertaion ***" << endl;
	cout << "Input Pipe ID " << inNode->outPipeId << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << "Function:" << endl;
	func.Print();
	cout << endl;
}

GroupByNode :: GroupByNode(Node *inNode, NameList *groupingAtts, FuncOperator *finalFunction) {
	Attribute sumAtt;
	char attName[5] = "sum";
	NameList *currAtt;
	vector<Attribute> atts;
	vector<int> attVector;

	this->inNode = inNode;
	inPipe = inNode->outPipe;

	currAtt = groupingAtts;
	while (currAtt != NULL) {
		int position = inNode->outSchema->Find(currAtt->name);
		Attribute att;
		if (position == -1) {
			cerr << "Attribute - " << currAtt->name << " is not present in the input schema!" << endl;
			exit(0);
		}

		att.name = currAtt->name;
		att.myType = inNode->outSchema->FindType(currAtt->name);
		atts.push_back(att);
		attVector.push_back(position);
		currAtt = currAtt->next;
	}
	reverse(atts.begin(), atts.end());
	reverse(attVector.begin(), attVector.end());

	order = new OrderMaker();
	for (int i = 0; i < atts.size(); i++) {
		order->whichAtts[i] = attVector[i];
		order->whichTypes[i] = atts[i].myType;
	}
	order->numAtts = atts.size();

	if (!finalFunction) {
		outSchema = inNode->outSchema;
	} else {
		func.GrowFromParseTree (finalFunction, *inNode->outSchema);
		sumAtt.name = attName;
		sumAtt.myType = func.ReturnType();
		atts.insert(atts.begin(), sumAtt);
		outSchema = new Schema("groupby_sch", atts.size(), atts.data());
	}

	groupby.Run(*inPipe, *outPipe, *order, func);
}

GroupByNode :: ~GroupByNode() {
	delete outSchema;
	delete order;
	delete inNode;
}

void GroupByNode :: Print() {
	inNode->Print();
	cout << "*** Group By opertaion ***" << endl;
	cout << "Input Pipe ID " << inNode->outPipeId << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << "Function:" << endl;
	func.Print();
	cout << endl;
}

ProjectNode :: ProjectNode(Node *inNode, NameList *attsList) {
	NameList *currAtt;

	this->inNode = inNode;
	inPipe = inNode->outPipe;
	inSchema = inNode->outSchema;

	currAtt = attsList;
	while (currAtt != NULL) {
		int position = inSchema->Find(currAtt->name);
		Attribute att;
		if (position == -1) {
			cerr << "Attribute - " << currAtt->name << " is not present in the input schema!" << endl;
			exit(0);
		}

		att.name = currAtt->name;
		att.myType = inSchema->FindType(currAtt->name);
		atts.push_back(att);
		attVector.push_back(position);
		currAtt = currAtt->next;
	}

	reverse(atts.begin(), atts.end());
	reverse(attVector.begin(), attVector.end());
	outSchema = new Schema("new_schema", atts.size(), atts.data());

	project.Run(*inPipe, *outPipe, attVector.data(), inSchema->GetNumAtts(), (int)attVector.size());
}

ProjectNode :: ~ProjectNode() {
	delete outSchema;
	delete inNode;
}

void ProjectNode :: Print() {
	inNode->Print();
	cout << "*** Project opertaion ***" << endl;
	cout << "Input Pipe ID " << inNode->outPipeId << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << "Project Attributes:" << endl;
	for (int i = 0; i < atts.size(); i++)
		cout << "\t" << atts[i].name << endl;
	cout << endl;
}

DupRemovalNode :: DupRemovalNode(Node *inNode) {
	this->inNode = inNode;
	inPipe = inNode->outPipe;
	outSchema = inNode->outSchema;

	dupRemoval.Run(*inPipe, *outPipe, *inNode->outSchema);
}

DupRemovalNode :: ~DupRemovalNode() {
	delete inNode;
}

void DupRemovalNode :: Print() {
	inNode->Print();
	cout << "*** Duplicate Removal opertaion ***" << endl;
	cout << "Input Pipe ID " << inNode->outPipeId << endl;
	cout << "Output Pipe ID " << outPipeId << endl;
	cout << "Output Schema:" << endl;
	outSchema->Print();
	cout << endl;
}

WriteOutNode :: WriteOutNode(Node *inNode, FILE *outFile) {
	this->inNode = inNode;
	this->inPipe = inNode->outPipe;
	this->outSchema = inNode->outSchema;

	out.Run(*inPipe, outFile, *outSchema);
}

WriteOutNode :: ~WriteOutNode() {
	delete inNode;
}

void WriteOutNode :: Print() {
	inNode->Print();
}

void WriteOutNode :: WaitUntilDone () {
	out.WaitUntilDone();
}
