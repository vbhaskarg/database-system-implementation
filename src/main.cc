#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <string>
#include <vector>
#include "QueryPlan.h"
#include "ParseTree.h"
#include "DBFile.h"
#include <ctime>
#include <iomanip>
#include <fstream>
#include <cstdio>


using namespace std;

#define RUN_LENGTH 100

extern "C" {
	int yyparse(void);   // defined in y.tab.c
}

extern int parserRequestCode;
extern char *parserTableName;
extern RelAttribute *parserTableSchema;
extern char *parserFileName;
extern int parserOutputCode;
extern struct NameList *sortingAtts;

char *tpch_rel[8] = {"region", "nation", "part", "supplier",
					 "partsupp", "customer", "orders", "lineitem"};

bool IsTablePresent(char *relName) {	
	ifstream in;
	string str;

	in.open(db_catalog_path, ofstream::in);
	if (!in.is_open())
		return false;

	in >> str;
	while(in.good()) {
		if (str == "BEGIN") {
			in >> str;
			if (!strcmp(str.data(), relName)) {
				return true;
			}
		}
		in >> str;
	}

	in.close();
	return false;
}

void AddTableToCatalog(char *relName, Attribute *atts, int numAtts) {
	ofstream out;

	out.open(db_catalog_path, ofstream::out | ofstream::app);
	if (!out.is_open()) {
		cout << "Error" << "catalog not opened" << endl;
	}

	out << endl;
	out << "BEGIN" << endl;
	out << relName << endl;
	out << relName << ".tbl" << endl;
	for (int i = 0; i < numAtts; i++)
		out << atts[i].name << " " << GetTypeStr(atts[i].myType) << endl;
	out << "END" << endl;
	out.close();
}

void InsertIntoTable(char *relName, char *tblFileName) {
	DBFile dbFile;
	char dbFileName[50];
	char tblFilePath[200];

	Schema tableSchema(db_catalog_path, relName);
	sprintf(dbFileName, "%s.bin", relName);
	sprintf(tblFilePath, "%s%s", tpch_dir, tblFileName);

	dbFile.Open(dbFileName);
	dbFile.Load(tableSchema, tblFilePath);
	dbFile.Close();
}

void DropTable(char *relName) {

	ifstream in;
	ofstream out;
	char temp[5] = "temp";
	string str, tableName, fileName;

	in.open(db_catalog_path, ofstream::in);
	if (!in.is_open())
		return;

	out.open(temp, ofstream::out | ofstream::trunc);

	in >> str;
	while(in.good()) {
		if (str == "BEGIN") {
			in >> tableName;
			in >> fileName;
			if (!strcmp(tableName.data(), relName)) {
				in >> str;
				while (in.good() && str != "BEGIN")
					in >> str;
				continue;
			}
			out << endl;
			out << "BEGIN" << endl;
			out << tableName << endl;
			out << fileName << endl;
			in >> str;
			while (str != "END") {
				out << str << " ";
				in >> str;
				out << str << endl;
				in >> str;
			}
			out << "END" << endl;
		}
		in >> str;
	}

	in.close();
	out.close();
	remove(db_catalog_path);
	rename(temp, db_catalog_path);
}

void CreateTpchTables() {
	char fileName[50];

	for (int i = 0; i < 8; i++) {
		DBFile dbFile;

		if (IsTablePresent(tpch_rel[i]))
			DropTable(tpch_rel[i]);
		Schema newSchema (catalog_path, tpch_rel[i]);
		AddTableToCatalog(tpch_rel[i], newSchema.GetAtts(), newSchema.GetNumAtts());
		
		sprintf(fileName, "%s.bin", tpch_rel[i]);
		dbFile.Create(fileName, heap, NULL);
		dbFile.Close();

		sprintf(fileName, "%s.tbl", tpch_rel[i]);
		InsertIntoTable(tpch_rel[i], fileName);
		cout << "\t" << tpch_rel[i] << " table created." << endl;
	}
	cout << endl << "All TPCH tables created." << endl << endl;
}

void CreateTable() {
	DBFile newDB;
	SortInfo sortInfo;
	Record literal;
	fType dbType = heap;
	char fileName[50];
	vector<Attribute> relAttVector;
	RelAttribute *currAtt;

	if (parserTableName == NULL) {
		CreateTpchTables();
		return;
	}

	if (IsTablePresent(parserTableName)) {
		cout << "Fail! " << parserTableName << " is already present in the DB." << endl;
		return;
	}

	currAtt = parserTableSchema;
	while (currAtt != NULL) {
		Attribute att;

		att.name = currAtt->name;
		att.myType = (Type)currAtt->type;
		relAttVector.push_back(att);
		currAtt = currAtt->next;
	}
	reverse(relAttVector.begin(), relAttVector.end());

	AddTableToCatalog(parserTableName, relAttVector.data(), relAttVector.size());

	sprintf(fileName, "%s.bin", parserTableName);

	/*if (sortingAtts != NULL) {
		Schema newSchema("new_schema", relAttVector.size(), relAttVector.data());
		CNF sort_pred;
		sort_pred.GrowFromParseTree (final, newSchema, literal);
		OrderMaker dummy;
		OrderMaker *sortorder = new OrderMaker();
		sort_pred.GetSortOrders (*sortorder, dummy);

		dbType = sorted;
		sortInfo.runLength = RUN_LENGTH;
		sortInfo.myOrder = sortorder;
	}*/

	newDB.Create(fileName, dbType, &sortInfo);
	newDB.Close();
}

void UpdateStatistics() {

}

int main () {
	string input;
	bool isActive = true;
	int output = STD_OUTPUT;
	char outputFile[50];
	clock_t begin_time;

	cout << "\t\t*** DB started ***" << endl << endl;

	while (isActive) {
		cout << "myDB=# ";
		yyparse();

		switch (parserRequestCode) {
			case SELECT_REQUEST: {
				QueryPlan qPlan;
				begin_time = clock();
				cout<< endl << endl;
				qPlan.Run(output, outputFile);
				cout << "Query execution time: " << fixed << setprecision(2);
				cout << ((double)(clock() - begin_time)/CLOCKS_PER_SEC) << " seconds.";
				cout<< endl << endl;
				break;
			}
			case CREATE_REQUEST:
				CreateTable();
				break;
			case INSERT_REQUEST:
				InsertIntoTable(parserTableName, parserFileName);
				break;
			case DROP_REQUEST:
				DropTable(parserTableName);
				break;
			case OUTPUT_REQUEST:
				output = parserOutputCode;
				sprintf(outputFile, "%s", parserFileName);
				break;
			case UPDATE_REQUEST:
				UpdateStatistics();
				break;
			case QUIT_REQUEST:
				isActive = false;
				break;
			default:
				cout << "Unknown request" << endl;
		}
	}

	return 0;
}
