#include <iostream>
#include <cstdlib>
#include "GenericDBFile.h"

HeapDBFile :: HeapDBFile () {
}

HeapDBFile :: ~HeapDBFile () {
}

void HeapDBFile :: FlushToDisk() {
	if (opMode == Write) {
		// Add page to the end of the file
		int dbPages = diskFile.GetLength()-1;
		if (dbPages < 0)
			dbPages = 0;
		diskFile.AddPage(&pageBuffer, dbPages);
		pageBuffer.EmptyItOut();
	}
}

void HeapDBFile :: Add (Record &rec) {
	if (opMode == Read)
		pageBuffer.EmptyItOut();

	// Do we need to handle dirty pages?

	if (!pageBuffer.Append(&rec)) {
		// pageBuffer is full. Flush it to disk and try adding the record again
		FlushToDisk();
		if(!pageBuffer.Append(&rec)) {
			cout << "Error: Failed to append record to page" << endl;
			exit(1);
		}
	}

	opMode = Write;
}

int HeapDBFile :: GetNext (Record &fetchme, CNF &cnf, Record &literal) {
	int result = 0;
	ComparisonEngine comp;

	while (!result && GenericDBFile::GetNext(fetchme))
		result = comp.Compare (&fetchme, &literal, &cnf);

	return result;
}
