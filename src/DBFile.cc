#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include "DBFile.h"
#include "Defs.h"

DBFile :: DBFile () {
	internalDBFile = NULL;
}

DBFile :: ~DBFile () {
}

void DBFile :: DBInit (fType f_type) {
	switch (f_type) {
		case heap:
			internalDBFile = new HeapDBFile();
			break;
		case sorted:
			internalDBFile = new SortedDBFile();
			break;
		default:
			cerr << "Error! Unknown DBFile type." << endl;
			exit(1);
	}
}

int DBFile :: Create (char *f_path, fType f_type, void *startup) {

	DBInit(f_type);

	// Save metadata path to write DB info while closing
	db_type = f_type;
	sprintf (db_metadata_path, "%s.meta", f_path);
	if (db_type == sorted) {
		memcpy(&sortInfo, startup, sizeof(SortInfo));
		sortInfo.myOrder = new OrderMaker();
		memcpy(sortInfo.myOrder, ((SortInfo *)startup)->myOrder, sizeof(OrderMaker));
	}

	return internalDBFile->Open(0, f_path, &sortInfo);
}

int DBFile :: Open (char *f_path) {
	// Save metadata path to write DB info while closing
	sprintf (db_metadata_path, "%s.meta", f_path);

	ifstream inStream (db_metadata_path, ios::binary);
	if (!inStream.is_open())
		cout << "Error: Metadata file open failed for " << f_path << endl;

	inStream.read((char *)&db_type, sizeof(fType));
	if (db_type == sorted) {
		inStream.read((char *)&sortInfo, sizeof(SortInfo));
		sortInfo.myOrder = new OrderMaker();
		inStream.read((char *)sortInfo.myOrder, sizeof(OrderMaker));
	}
	inStream.close();

	DBInit(db_type);

	return internalDBFile->Open(1, f_path, &sortInfo);
}

int DBFile :: Close () {
	int result = internalDBFile->Close();
	delete internalDBFile;
	internalDBFile = NULL;

	ofstream outStream (db_metadata_path, ios::trunc | ios::binary);
	outStream.write((char *)&db_type, sizeof(fType));
	if (db_type == sorted) {
		outStream.write((char *)&sortInfo, sizeof(SortInfo));
		outStream.write((char *)sortInfo.myOrder, sizeof(OrderMaker));
		delete sortInfo.myOrder;
	}
	outStream.close();

	return result;
}

void DBFile :: Load (Schema &f_schema, char *loadpath) {
	internalDBFile->Load(f_schema, loadpath);
}

void DBFile :: MoveFirst () {
	internalDBFile->MoveFirst();
}

void DBFile :: Add (Record &rec) {
	internalDBFile->Add(rec);
}

int DBFile :: GetNext (Record &fetchme) {
	return internalDBFile->GetNext(fetchme);
}

int DBFile :: GetNext (Record &fetchme, CNF &cnf, Record &literal) {
	return internalDBFile->GetNext(fetchme, cnf, literal);
}
