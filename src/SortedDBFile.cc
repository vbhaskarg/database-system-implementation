#include <iostream>
#include <cstring>
#include "GenericDBFile.h"

SortedDBFile :: SortedDBFile () {
	buffsz = 100;
	inPipe = NULL;
	outPipe = NULL;
	bigQ = NULL;
}

SortedDBFile :: ~SortedDBFile () {
}

void SortedDBFile :: SetupBigQ () {
	inPipe = new Pipe(buffsz);
	outPipe = new Pipe(buffsz);
	bigQ = new BigQ(*inPipe, *outPipe, *sortInfo->myOrder, sortInfo->runLength);
}

void SortedDBFile :: FlushToDisk() {
	Record rec1, rec2;
	HeapDBFile tempFile, heapFile;
	ComparisonEngine ceng;
	bool run1, run2;

	if (opMode != Write)
		return;

	inPipe->ShutDown();
	diskFile.Close();

	tempFile.Open (0, "temp", NULL);
	heapFile.Open (1, dbPath, NULL);
	heapFile.MoveFirst();

	run1 = (bool)outPipe->Remove(&rec1);
	run2 = (bool)heapFile.GenericDBFile::GetNext(rec2);
	while (run1 && run2) {
		if (ceng.Compare (&rec1, &rec2, sortInfo->myOrder) > 0) {
			tempFile.Add(rec2);
			run2 = (bool)heapFile.GenericDBFile::GetNext(rec2);
		} else {
			tempFile.Add(rec1);
			run1 = (bool)outPipe->Remove(&rec1);
		}
	}

	if (run1) {
		tempFile.Add(rec1);
		while (outPipe->Remove(&rec1))
			tempFile.Add(rec1);
	}

	if (run2) {
		tempFile.Add(rec2);
		while (heapFile.GenericDBFile::GetNext(rec2))
			tempFile.Add(rec2);
	}

	tempFile.Close();
	heapFile.Close();

	// temp file is the new DB.
	remove(dbPath);
	rename("temp", dbPath);

	diskFile.Open(1, dbPath);

	delete inPipe;
	delete outPipe;
	delete bigQ;
	inPipe = NULL;
	outPipe = NULL;
	bigQ = NULL;
}

void SortedDBFile :: Add (Record &rec) {
	if (opMode != Write)
		SetupBigQ();

	inPipe->Insert (&rec);

	opMode = Write;
}

void SortedDBFile :: BuildQueryOrder (CNF &cnf) {
	bool flag;

	// initialize the size of the queryOrder
	qInfo.queryOrder.numAtts = 0;

	// loop through all the attributes in the sortOrder
	for (int i = 0; i < sortInfo->myOrder->numAtts; i++) {
		flag = false;

		// For each attribute in sortOrder, look if it is present in
		// any of the subexpressions (disjunctions) in the CNF instance
		// that you are asked to search on.
		for (int j = 0; j < cnf.numAnds; j++) {

			// Make sure it is the only attribute present in this subexpression,
			// otherwise it cannot be added to query OrderMaker
			if (cnf.orLens[j] != 1)
				continue;

			// Verify that it is an equality check
			if (cnf.orList[j][0].op != Equals)
				continue;

			// If this attribute is in the CNF instance,
			// then add it to the end of the queryOrder
			if (cnf.orList[j][0].operand1 == Left &&
				cnf.orList[j][0].operand2 == Literal &&
				sortInfo->myOrder->whichAtts[i] == cnf.orList[j][0].whichAtt1 &&
				sortInfo->myOrder->whichTypes[i] == cnf.orList[j][0].attType) {

				qInfo.queryOrder.whichAtts[qInfo.queryOrder.numAtts] = sortInfo->myOrder->whichAtts[i];
				qInfo.queryOrder.whichTypes[qInfo.queryOrder.numAtts] = sortInfo->myOrder->whichTypes[i];
				qInfo.attPosition[sortInfo->myOrder->whichAtts[i]] = cnf.orList[j][0].whichAtt2;
				qInfo.queryOrder.numAtts++;
				flag = true;
				break;
			} else if (cnf.orList[j][0].operand2 == Left &&
				cnf.orList[j][0].operand1 == Literal &&
				sortInfo->myOrder->whichAtts[i] == cnf.orList[j][0].whichAtt2 &&
				sortInfo->myOrder->whichTypes[i] == cnf.orList[j][0].attType) {

				qInfo.queryOrder.whichAtts[qInfo.queryOrder.numAtts] = sortInfo->myOrder->whichAtts[i];
				qInfo.queryOrder.whichTypes[qInfo.queryOrder.numAtts] = sortInfo->myOrder->whichTypes[i];
				qInfo.attPosition[sortInfo->myOrder->whichAtts[i]] = cnf.orList[j][0].whichAtt1;
				qInfo.queryOrder.numAtts++;
				flag = true;
				break;
			}
		}

		if (!flag)
			break;
	}
}

int SortedDBFile :: GetMatchingRec(int index, Record &fetchme, CNF &cnf, Record &literal) {
	ComparisonEngine comp;
	int result;

	dbPageIndex = index;
	pageBuffer.EmptyItOut();

	while (GenericDBFile::GetNext(fetchme)) {
		result = CompareLiteral(&fetchme, &literal);
		if (result > 0)
			break;
		else if (result == 0 && comp.Compare (&fetchme, &literal, &cnf) == 1)
			return 1;
	}

	return 0;
}

int SortedDBFile :: GetFirstRecord(int pageIndex, Record &fetchme) {
	pageBuffer.EmptyItOut();
	diskFile.GetPage(&pageBuffer, pageIndex);
	if(!pageBuffer.GetFirst(&fetchme))
		return 0;

	return 1;
}

int SortedDBFile :: BinarySearch(Record &fetchme, CNF &cnf, Record &literal) {
	int startPage, endPage, midPage, currPage;
	int index;
	int numPages = diskFile.GetLength()-1;
	int result;

	startPage = 0;
	endPage = numPages-1;

	while(startPage <= endPage) {
		if (startPage == endPage)
			return GetMatchingRec(startPage, fetchme, cnf, literal);

		midPage = (startPage + endPage)/2;

		GetFirstRecord(midPage, fetchme);

		result = CompareLiteral(&fetchme, &literal);
		if (result == 0) {
			currPage = midPage-1;
			while(currPage >= startPage) {
				GetFirstRecord(currPage, fetchme);
				if (CompareLiteral(&fetchme, &literal) == 0) {
					currPage--;
					continue;
				} else
					break;
			}

			return GetMatchingRec(currPage, fetchme, cnf, literal);
		} else if (result > 0) {
			endPage = midPage-1;
		} else {
			GetFirstRecord(midPage+1, fetchme);
			if (CompareLiteral(&fetchme, &literal) < 0)
				startPage = midPage+1;
			else
				return GetMatchingRec(midPage, fetchme, cnf, literal);
		}
	}

	return 0;
}

// returns a -1, 0, or 1 depending upon whether given record is less then,
// equal to, or greater than literal record, depending upon the OrderMaker
int SortedDBFile :: CompareLiteral(Record *record, Record *literal) {

	char *val1, *val2;
	int attPosition;

	char *record_bits = record->GetBits();
	char *literal_bits = literal->GetBits();

	for (int i = 0; i < qInfo.queryOrder.numAtts; i++) {
		val1 = record_bits + ((int *) record_bits)[qInfo.queryOrder.whichAtts[i] + 1];

		// Attribute position in literal
		attPosition = qInfo.attPosition[qInfo.queryOrder.whichAtts[i]];
		val2 = literal_bits + ((int *) literal_bits)[attPosition + 1];

		// these are used to store the two operands, depending on their type
		int val1Int, val2Int;
		double val1Double, val2Double;

		// now check the type and do the comparison
		switch (qInfo.queryOrder.whichTypes[i]) {

			// first case: we are dealing with integers
			case Int:

			// cast the two bit strings to ints
			val1Int = *((int *) val1);
			val2Int = *((int *) val2);

			// and do the comparison
			if (val1Int < val2Int)
				return -1;
			else if (val1Int > val2Int)
				return 1;

			break;

			// second case: dealing with doubles
			case Double:

			// cast the two bit strings to doubles
			val1Double = *((double *) val1);
			val2Double = *((double *) val2);

			// and do the comparison
			if (val1Double < val2Double)
				return -1;
			else if (val1Double > val2Double)
				return 1;

			break;

			// last case: dealing with strings
			default:
			int sc = strcmp (val1, val2);
			if (sc != 0)
				return sc > 0 ? 1 : -1;

			break;
		}
	}

	return 0;
}

int SortedDBFile :: GetNext (Record &fetchme, CNF &cnf, Record &literal) {
	ComparisonEngine comp;
	int result = 0;

	if (isNewCnf) {
		BuildQueryOrder(cnf);
		isNewCnf = false;

		if (qInfo.queryOrder.numAtts > 0) {
			if (opMode == Write)
				FlushToDisk();
			opMode = Read;
			return BinarySearch(fetchme, cnf, literal);
		}
	}

	while (!result && GenericDBFile::GetNext(fetchme)) {
		if (qInfo.queryOrder.numAtts > 0 && CompareLiteral (&fetchme, &literal) != 0)
			break;
		result = comp.Compare(&fetchme, &literal, &cnf);
	}

	return result;
}
