#include "BigQ.h"
#include <algorithm>
#include <cstdio>

struct BigQCompare {
	OrderMaker *bigQSortOrder;
	bool operator() (Record *a, Record *b) {
		ComparisonEngine ceng;
		return ceng.Compare (a, b, bigQSortOrder) < 0 ? 1 : 0;
	}
	BigQCompare(OrderMaker *bigQSortOrder) { this->bigQSortOrder = bigQSortOrder; }
};

BigQ :: BigQ (Pipe &in, Pipe &out, OrderMaker &sortorder, int runlen) {

	nOfRuns = 0;
	inPipeRef = &in;
	outPipeRef = &out;
	sortorderRef = &sortorder;
	this->runlen = runlen;

	// thread to sort the records
	pthread_t bigQThread;
	pthread_create (&bigQThread, NULL, &BigQ::SortRecords_helper, this);
}

BigQ :: ~BigQ () {
}

void *BigQ :: SortRecords_helper(void *context) {
	((BigQ *)context)->SortRecords();
	pthread_exit(NULL);
}

void *BigQ :: SortRecords() {

	// new temp file for each instance
	char str[6];
	GetRandomStr(str, 5);

	bigQFile.Open(0, str);

	// How much do we need to reserve?
	// bigQVec.reserve(PAGE_SIZE * runlen);

	// read data from in pipe sort them into runlen pages
	GenerateRuns();

	// construct priority queue over sorted runs and dump sorted data
	// into the out pipe
	MergeRuns();

	bigQFile.Close();

	// Delete the temp file on the disk
	remove(str);

	// finally shut down the out pipe
	outPipeRef->ShutDown ();
}

bool BigQ :: AddToVector() {
	Record rec;
	bool ret = false;

	while (bigQPage.GetFirst(&rec)) {
		Record *temp = new Record();
		temp->Consume(&rec);
		bigQVec.push_back(temp);
		ret = true;
	}

	return ret;
}

void BigQ :: flushToDisk() {
	// Add page to the end of the file
	int filePages = bigQFile.GetLength()-1;
	if (filePages < 0)
		filePages = 0;

	bigQFile.AddPage(&bigQPage, filePages);
	bigQPage.EmptyItOut();
}

void BigQ :: sortAndWriteRun() {
	int currLen = 0;

	if (bigQVec.size() == 0)
		return;

	sort(bigQVec.begin(), bigQVec.end(), BigQCompare(sortorderRef));

	for (int i = 0; i < bigQVec.size(); i++) {
		while (!bigQPage.Append(bigQVec.at(i))) {
			flushToDisk();
			currLen++;
		}
		delete bigQVec.at(i);
	}

	// Check back: Need to handle this appropriately.
	if (currLen < runlen) {
		flushToDisk();
	}

	nOfRuns++;
	bigQVec.clear();
}

void BigQ :: GenerateRuns() {
	Record rec;
	int currLen = 0;

	while (inPipeRef->Remove(&rec)) {
		while(!bigQPage.Append(&rec)) {
			// Add records to vector whenever page is full
			if (!AddToVector())
				cout << __func__ << ": Record size greater than page size" << endl;

			currLen++;
			if (currLen == runlen) {
				sortAndWriteRun();
				currLen = 0;
			}
		}
	}

	while (AddToVector())
		sortAndWriteRun();

	vector<Record *> temp;
	// Shrinks the capacity of biqQVec to zero.
	bigQVec.swap(temp);
}

void BigQ :: MergeSetup() {
	for (int i = 0; i < nOfRuns; i++) {
		bigQFile.GetPage(&runArray[i].currPage, i * runlen);
		runArray[i].currLen = 1;
		runHeadPtr[i] = new Record();
		if (!runArray[i].currPage.GetFirst(runHeadPtr[i])) {
			delete runHeadPtr[i];
			runHeadPtr[i] = NULL;
		}
	}
}

bool BigQ :: GetNextRecord(Record *recPtr) {
	int start = 0;
	int min = -1;
	ComparisonEngine ceng;

	while (start < nOfRuns) {
		if (runHeadPtr[start] != NULL) {
			min = start;
			break;
		}
		start++;
	}

	if (min == -1)
		return false;

	// Find the min record from the run heads
	for (int i = start+1; i < nOfRuns; i++) {
		if (runHeadPtr[i] != NULL &&
			ceng.Compare (runHeadPtr[min], runHeadPtr[i], sortorderRef) == 1)
			min = i;
	}

	recPtr->Consume(runHeadPtr[min]);

	// Get the next record in the run to run head
	if (!runArray[min].currPage.GetFirst(runHeadPtr[min])) {
		int nextPageIndex = min * runlen + runArray[min].currLen;

		if (runArray[min].currLen < runlen && nextPageIndex < bigQFile.GetLength()-1) {
			bigQFile.GetPage(&runArray[min].currPage, nextPageIndex);
			runArray[min].currLen++;
			if (!runArray[min].currPage.GetFirst(runHeadPtr[min])) {
				delete runHeadPtr[min];
				runHeadPtr[min] = NULL;
			}
		} else {
			delete runHeadPtr[min];
			runHeadPtr[min] = NULL;
		}
	}

	return true;
}

void BigQ :: MergeRuns() {
	Record rec;

	runHeadPtr = new Record *[nOfRuns]();
	runArray = new RunStatus[nOfRuns];

	MergeSetup();

	while (GetNextRecord(&rec))
		outPipeRef->Insert (&rec);

	delete [] runHeadPtr;
	delete [] runArray;
}
