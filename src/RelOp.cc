#include "RelOp.h"
#include <cstring>

void RelationalOp::WaitUntilDone () {
	pthread_join (opThread, NULL);
}

void RelationalOp::Use_n_Pages (int runlen) {
	this->runlen = runlen;
}

void *RelationalOp::RunThreadHelper (void *context) {
	((RelationalOp *)context)->RunThread();
	pthread_exit(NULL);
}

void SelectFile::Run (DBFile &inFile, Pipe &outPipe, CNF &selOp, Record &literal) {
	inFilePtr = &inFile;
	outPipePtr = &outPipe;
	selOpPtr = &selOp;
	literalPtr = &literal;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void SelectFile::RunThread () {
	Record rec;

	inFilePtr->MoveFirst();
	while(inFilePtr->GetNext(rec, *selOpPtr, *literalPtr) == 1)
		outPipePtr->Insert(&rec);

	outPipePtr->ShutDown();
}

void SelectPipe::Run (Pipe &inPipe, Pipe &outPipe, CNF &selOp, Record &literal) {
	inPipePtr = &inPipe;
	outPipePtr = &outPipe;
	selOpPtr = &selOp;
	literalPtr = &literal;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void SelectPipe::RunThread () {
	Record rec;
	ComparisonEngine comp;

	while (inPipePtr->Remove(&rec)) {
		if (comp.Compare(&rec, literalPtr, selOpPtr) == 1)
			outPipePtr->Insert(&rec);
	}

	outPipePtr->ShutDown();
}

void Project::Run (Pipe &inPipe, Pipe &outPipe, int *keepMe, int numAttsInput, int numAttsOutput) {
	inPipePtr = &inPipe;
	outPipePtr = &outPipe;
	attsArray = keepMe;
	this->numAttsInput = numAttsInput;
	this->numAttsOutput = numAttsOutput;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void Project::RunThread () {
	Record rec;

	while (inPipePtr->Remove(&rec)) {
		rec.Project(attsArray, numAttsOutput, numAttsInput);
		outPipePtr->Insert(&rec);
	}

	outPipePtr->ShutDown();
}

void Join::ComputeMergeParams(Record &leftRec, Record &rightRec) {
	numAttsLeft = leftRec.GetNumAtts();
	numAttsRight = rightRec.GetNumAtts();
	numAttsToKeep = numAttsLeft+numAttsRight;
	startOfRight = numAttsLeft;
	attsToKeep = new int[numAttsToKeep];

	for (int i = 0; i < startOfRight; i++)
		attsToKeep[i] = i;
	for (int i = startOfRight; i < numAttsToKeep; i++)
		attsToKeep[i] = i-startOfRight;
}

void Join::JoinBuffRecs () {

	Record rec;
	int count = 0;

	for (int i = 0; i < leftRecCount; i++) {
		for (int j = 0; j < rightRecCount; j++) {
			if (comp.Compare(&leftRecBuff[i], &rightRecBuff[j], literalPtr, selOpPtr) == 1) {
				rec.MergeRecords (&leftRecBuff[i], &rightRecBuff[j], numAttsLeft, numAttsRight,
					attsToKeep, numAttsToKeep, startOfRight);
				outPipePtr->Insert(&rec);
				count++;
			}
		}
	}
	cout << "Nested Loop Join: Inserted " << count << " records in out pipe" << endl;
	rightRecCount = 0;
}

void Join::JoinWithRightRecs () {

	Record rightRec;

	rightRecDB.MoveFirst();
	while (rightRecDB.GetNext(rightRec)) {
		if (rightRecCount == maxRecs)
			JoinBuffRecs();

		rightRecBuff[rightRecCount].Consume(&rightRec);
		rightRecCount++;
	}
	if (rightRecCount > 0)
		JoinBuffRecs();

	leftRecCount = 0;
}

void Join::NestedLoopJoin () {
	Record leftRec, rightRec, rec;

	leftRecCount = 0;
	rightRecCount = 0;

	char rightBuffName[6];
	GetRandomStr(rightBuffName, 5);
	rightRecDB.Create(rightBuffName, heap, NULL);
	leftRecBuff = new Record[maxRecs];
	rightRecBuff = new Record[maxRecs];

	isLeftEmpty = leftPipePtr->Remove(&leftRec) == 0 ? true : false;
	isRightEmpty = rightPipePtr->Remove(&rightRec) == 0 ? true : false;
	if (isLeftEmpty || isRightEmpty) {
		outPipePtr->ShutDown();
		return;
	}

	ComputeMergeParams(leftRec, rightRec);

	while (!isRightEmpty) {
		rightRecDB.Add(rightRec);
		isRightEmpty = rightPipePtr->Remove(&rightRec) == 0 ? true : false;
	}

	while (!isLeftEmpty) {
		if (leftRecCount == maxRecs)
			JoinWithRightRecs();

		leftRecBuff[leftRecCount].Consume(&leftRec);
		leftRecCount++;
		isLeftEmpty = leftPipePtr->Remove(&leftRec) == 0 ? true : false;
	}
	if (leftRecCount > 0)
		JoinWithRightRecs();

	rightRecDB.Close();
	delete[] leftRecBuff;
	delete[] rightRecBuff;
}

void Join::SortMergeJoin (OrderMaker &leftOrder, OrderMaker &rightOrder) {
	Record leftRec, rightRec, rec;

	rightRecBuff = new Record[maxRecs];

	Pipe leftOut(100), rightOut(100);
	BigQ leftBigQ (*leftPipePtr, leftOut, leftOrder, runlen);
	BigQ rightBigQ (*rightPipePtr, rightOut, rightOrder, runlen);

	isRightEmpty = rightOut.Remove(&rightRec) == 0 ? true : false;
	isLeftEmpty = leftOut.Remove(&leftRec) == 0 ? true : false;

	ComputeMergeParams(leftRec, rightRec);

	while (!isLeftEmpty && !isRightEmpty) {
		int result = comp.Compare(&leftRec, &leftOrder, &rightRec, &rightOrder);
		if (result > 0) {
			isRightEmpty = rightOut.Remove(&rightRec) == 0 ? true : false;
		} else if (result < 0) {
			isLeftEmpty = leftOut.Remove(&leftRec) == 0 ? true : false;
		} else {
			int recordCount = 0;
			Record prevLeftRec, prevRightRec;

			prevRightRec.Copy(&rightRec);
			do {
				if (recordCount+1 > (maxRecs)) {
					cerr << "Error in Join: Exceeded internal record buffer size" << endl;
					exit(0);
				}

				rightRecBuff[recordCount].Consume(&rightRec);
				recordCount++;
			} while (!(isRightEmpty = rightOut.Remove(&rightRec) == 0 ? true : false) &&
				comp.Compare(&prevRightRec, &rightRec, &rightOrder) == 0);

			prevLeftRec.Copy(&leftRec);
			do {
				for (int i = 0; i < recordCount; i++) {
					if (comp.Compare(&leftRec, &rightRecBuff[i], literalPtr, selOpPtr) == 1) {
						rec.MergeRecords (&leftRec, &rightRecBuff[i], numAttsLeft, numAttsRight,
							attsToKeep, numAttsToKeep, startOfRight);
						outPipePtr->Insert(&rec);
					}
				}
			} while (!(isLeftEmpty = leftOut.Remove(&leftRec) == 0 ? true : false) &&
				comp.Compare(&prevLeftRec, &leftRec, &leftOrder) == 0);
		}
	}

	while (!isLeftEmpty)
		isLeftEmpty = leftOut.Remove(&leftRec) == 0 ? true : false;
	while (!isRightEmpty)
		isRightEmpty = rightOut.Remove(&rightRec) == 0 ? true : false;

	delete[] rightRecBuff;
}

void Join::RunThread () {
	OrderMaker leftOrder, rightOrder;

	if (runlen <= 0 || runlen > 1000)
		runlen = 100;

	maxRecs = runlen * 1000;
	int val = selOpPtr->GetSortOrders(leftOrder, rightOrder);
	// val = 0; // Try Nested Loop Join
	if (val > 0)
		SortMergeJoin(leftOrder, rightOrder);
	else
		NestedLoopJoin();

	outPipePtr->ShutDown();
}

void Join::Run (Pipe &inPipeL, Pipe &inPipeR, Pipe &outPipe, CNF &selOp, Record &literal) {
	leftPipePtr = &inPipeL;
	rightPipePtr = &inPipeR;
	outPipePtr = &outPipe;
	selOpPtr = &selOp;
	literalPtr = &literal;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void DuplicateRemoval::Run (Pipe &inPipe, Pipe &outPipe, Schema &mySchema) {
	inPipePtr = &inPipe;
	outPipePtr = &outPipe;
	schemaPtr = &mySchema;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void DuplicateRemoval::RunThread () {
	Pipe bigqOut(100);
	OrderMaker myOrder(schemaPtr);
	Record currRec, prevRec;
	ComparisonEngine comp;

	if (runlen <= 0 || runlen > 1000)
		runlen = 100;

	BigQ bigq (*inPipePtr, bigqOut, myOrder, runlen);

	// Make the first record as previous record.
	if (bigqOut.Remove(&currRec)) {
		prevRec.Copy(&currRec);
		outPipePtr->Insert(&currRec);
	}

	while(bigqOut.Remove(&currRec)) {
		if (comp.Compare (&prevRec, &currRec, &myOrder) != 0) {
			prevRec.Copy(&currRec);
			outPipePtr->Insert(&currRec);
		}
	}

	outPipePtr->ShutDown();
}

void Sum::Run (Pipe &inPipe, Pipe &outPipe, Function &computeMe) {
	inPipePtr = &inPipe;
	outPipePtr = &outPipe;
	functionPtr = &computeMe;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void Sum::RunThread () {
	Record rec;
	int intVal = 0;
	double doubleVal = 0;
	long intSum = 0;
	long double doubleSum = 0;
	Type resultType;
	Attribute intAtt = {"sum", Int};
	Attribute doubleAtt = {"sum", Double};
	char sum[50];
	int numRecs = 0;

	while (inPipePtr->Remove(&rec)) {
		resultType = functionPtr->Apply(rec, intVal, doubleVal);
		if (resultType == Int)
			intSum += intVal;
		else
			doubleSum += doubleVal;
		numRecs++;
	}

	if (numRecs == 0) {
		outPipePtr->ShutDown();
		return;
	}

	if (resultType == Int) {
		Schema *intSchema = new Schema ("res_sch", 1, &intAtt);
		sprintf(sum, "%ld|", intSum);
		rec.ComposeRecord(intSchema, (const char *)sum);
		delete intSchema;
	} else {
		Schema *doubleSchema = new Schema ("res_sch", 1, &doubleAtt);
		sprintf(sum, "%Lf|", doubleSum);
		rec.ComposeRecord(doubleSchema, (const char *)sum);
		delete doubleSchema;
	}

	outPipePtr->Insert(&rec);

	outPipePtr->ShutDown();
}

void GroupBy::Run (Pipe &inPipe, Pipe &outPipe, OrderMaker &groupAtts, Function &computeMe) {
	inPipePtr = &inPipe;
	outPipePtr = &outPipe;
	myOrderPtr = &groupAtts;
	functionPtr = &computeMe;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void GroupBy::StartNewGroupSum () {
	prevRec.Copy(&currRec);

	sumIn = new Pipe(100);
	sumOut = new Pipe(100);
	sum.Run(*sumIn, *sumOut, *functionPtr);
	sumIn->Insert(&currRec);
}

void GroupBy::WritePrevGroupSum (int numAttsLeft, int numAttsRight,
	int *attsToKeep, int numAttsToKeep, int startOfRight) {

	sumIn->ShutDown();
	sum.WaitUntilDone();
	sumOut->Remove(&sumRec);

	// Merge sum with grouping attributes
	outRec.MergeRecords(&sumRec, &prevRec, numAttsLeft,
		numAttsRight, attsToKeep, numAttsToKeep, startOfRight);
	outPipePtr->Insert(&outRec);

	delete sumIn, sumOut;
}

void GroupBy::RunThread () {
	Pipe bigqOut(100);
	ComparisonEngine comp;
	int numAttsLeft, numAttsRight, numAttsToKeep, startOfRight;
	int *attsToKeep;

	if (runlen <= 0 || runlen > 1000)
		runlen = 100;

	BigQ bigq (*inPipePtr, bigqOut, *myOrderPtr, runlen);

	if (!functionPtr->isInitialized) {
		while (bigqOut.Remove(&currRec))
			outPipePtr->Insert(&currRec);
	} else {
		// Make the first record as previous record.
		if (bigqOut.Remove(&currRec)) {
			numAttsLeft = 1;
			numAttsRight = currRec.GetNumAtts();
			numAttsToKeep = 1+myOrderPtr->numAtts;
			startOfRight = 1;
			attsToKeep = new int[numAttsToKeep];
			attsToKeep[0] = 0;
			for (int i = startOfRight; i < numAttsToKeep; i++)
				attsToKeep[i] = myOrderPtr->whichAtts[i-startOfRight];

			StartNewGroupSum();

			while(bigqOut.Remove(&currRec)) {
				if (comp.Compare (&prevRec, &currRec, myOrderPtr) == 0) {
					sumIn->Insert(&currRec);
				} else {
					WritePrevGroupSum(numAttsLeft, numAttsRight,
						attsToKeep, numAttsToKeep, startOfRight);
					StartNewGroupSum();
				}
			}

			WritePrevGroupSum(numAttsLeft, numAttsRight,
				attsToKeep, numAttsToKeep, startOfRight);
		}
	}

	outPipePtr->ShutDown();
}

void WriteOut::Run (Pipe &inPipe, FILE *outFile, Schema &mySchema) {
	inPipePtr = &inPipe;
	outFilePtr = outFile;
	schemaPtr = &mySchema;

	pthread_create (&opThread, NULL, &RelationalOp::RunThreadHelper, this);
}

void WriteOut::RunThread () {
	Record rec;
	char recordStr[PAGE_SIZE];
	int count = 0;

	while (inPipePtr->Remove(&rec)) {
		memset (recordStr, 0, PAGE_SIZE);
		rec.toString(schemaPtr, recordStr);
		if (outFilePtr != NULL)
			fputs(recordStr, outFilePtr);
		else
			cout << recordStr;
		count++;
	}
	cout << count << " records removed from out pipe." << endl;
	if (outFilePtr != NULL)
		fclose(outFilePtr);
}
