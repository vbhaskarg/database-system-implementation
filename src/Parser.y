 
%{

	#include "ParseTree.h" 
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include <iostream>

	extern "C" int yylex();
	extern "C" int yyparse();
	extern "C" void yyerror(char *s);
 
	// this is the final parse tree that is returned	
	struct AndList *final;

	// these data structures hold the result of the parsing
	int parserRequestCode;
	char *parserTableName; // name of the table
	RelAttribute *parserTableSchema; // Schema of the table
	char *parserFileName;
	int parserOutputCode;
	struct NameList *sortingAtts;
	struct FuncOperator *finalFunction; // the aggregate function (NULL if no agg)
	struct TableList *tables; // the list of tables and aliases in the query
	struct AndList *boolean; // the predicate in the WHERE clause
	struct NameList *groupingAtts; // grouping atts (NULL if no grouping)
	struct NameList *attsToSelect; // the set of attributes in the SELECT (NULL if no such atts)
	int distinctAtts; // 1 if there is a DISTINCT in a non-aggregate query 
	int distinctFunc;  // 1 if there is a DISTINCT in an aggregate query

%}

// this stores all of the types returned by production rules
%union {
 	struct FuncOperand *myOperand;
	struct FuncOperator *myOperator; 
	struct TableList *myTables;
	struct ComparisonOp *myComparison;
	struct Operand *myBoolOperand;
	struct OrList *myOrList;
	struct AndList *myAndList;
	struct NameList *myNames;
	struct RelAttribute *myAttributes;
	char *actualChars;
	char whichOne;
}

%token <actualChars> Name
%token <actualChars> Float
%token <actualChars> Int
%token <actualChars> String
%token CREATE
%token TABLE
%token HEAP
%token SORTED
%token ON
%token INSERT
%token INTO
%token DROP
%token SET
%token OUTPUT
%token STDOUT
%token NONE
%token UPDATE
%token STATISTICS
%token INTEGER
%token DOUBLE
%token STRING
%token FOR
%token SELECT
%token GROUP 
%token DISTINCT
%token BY
%token FROM
%token WHERE
%token SUM
%token AS
%token AND
%token OR
%token ALL
%token quit

%type <myOrList> OrList
%type <myAndList> AndList
%type <myOperand> SimpleExp
%type <myOperator> CompoundExp
%type <myAttributes> MySchema
%type <whichOne> Op 
%type <myComparison> BoolComp
%type <myComparison> Condition
%type <myTables> Tables
%type <myBoolOperand> Literal
%type <myNames> Atts

%start SQL


//******************************************************************************
// SECTION 3
//******************************************************************************
/* This is the PRODUCTION RULES section which defines how to "understand" the 
 * input language and what action to take for each "statment"
 */

%%

SQL: SELECT WhatIWant FROM Tables WHERE AndList
{
	parserRequestCode = SELECT_REQUEST;
	tables = $4;
	boolean = $6;	
	groupingAtts = NULL;
}

| SELECT WhatIWant FROM Tables WHERE AndList GROUP BY Atts
{
	parserRequestCode = SELECT_REQUEST;
	tables = $4;
	boolean = $6;	
	groupingAtts = $9;
}

| CREATE TABLE Name '(' MySchema ')' AS HEAP
{
	parserRequestCode = CREATE_REQUEST;
	parserTableName = $3;
	parserTableSchema = $5;
	sortingAtts = NULL;
}

| CREATE TABLE Name '(' MySchema ')' AS SORTED ON Atts
{
	parserRequestCode = CREATE_REQUEST;
	parserTableName = $3;
	parserTableSchema = $5;
	sortingAtts = $10;
}

| CREATE TABLE ALL
{
	parserRequestCode = CREATE_REQUEST;
	parserTableName = NULL;
	parserTableSchema = NULL;
	sortingAtts = NULL;
}

| INSERT Name INTO Name
{
	parserRequestCode = INSERT_REQUEST;
	parserFileName = $2;
	parserTableName = $4;
}

| DROP TABLE Name
{
	parserRequestCode = DROP_REQUEST;
	parserTableName = $3;
}

| SET OUTPUT STDOUT
{
	parserRequestCode = OUTPUT_REQUEST;
	parserOutputCode = STD_OUTPUT;
	parserFileName = NULL;
}

| SET OUTPUT Name
{
	parserRequestCode = OUTPUT_REQUEST;
	parserOutputCode = FILE_OUTPUT;
	parserFileName = $3;
}

| SET OUTPUT NONE
{
	parserRequestCode = OUTPUT_REQUEST;
	parserOutputCode = NO_OUTPUT;
	parserFileName = NULL;
}

| UPDATE STATISTICS FOR Name
{
	parserRequestCode = UPDATE_REQUEST;
	parserTableName = $4;
}

| AndList
{
	boolean = $1;
}

| quit
{
	parserRequestCode = QUIT_REQUEST;
}
;

MySchema: Name INTEGER
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $1;
	$$->type = 0;
	$$->next = NULL;
}

| Name DOUBLE
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $1;
	$$->type = 1;
	$$->next = NULL;
}

| Name STRING
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $1;
	$$->type = 2;
	$$->next = NULL;
}

| MySchema ',' Name INTEGER
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $3;
	$$->type = 0;
	$$->next = $1;
}

| MySchema ',' Name DOUBLE
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $3;
	$$->type = 1;
	$$->next = $1;
}

| MySchema ',' Name STRING
{
	$$ = (struct RelAttribute *) malloc (sizeof (struct RelAttribute));
	$$->name = $3;
	$$->type = 2;
	$$->next = $1;
}
;

WhatIWant: Function ',' Atts 
{
	attsToSelect = $3;
	distinctAtts = 0;
}

| Function
{
	attsToSelect = NULL;
}

| Atts 
{
	distinctAtts = 0;
	finalFunction = NULL;
	attsToSelect = $1;
}

| DISTINCT Atts
{
	distinctAtts = 1;
	finalFunction = NULL;
	attsToSelect = $2;
	finalFunction = NULL;
};

Function: SUM '(' CompoundExp ')'
{
	distinctFunc = 0;
	finalFunction = $3;
}

| SUM DISTINCT '(' CompoundExp ')'
{
	distinctFunc = 1;
	finalFunction = $4;
};

Atts: Name
{
	$$ = (struct NameList *) malloc (sizeof (struct NameList));
	$$->name = $1;
	$$->next = NULL;
} 

| Atts ',' Name
{
	$$ = (struct NameList *) malloc (sizeof (struct NameList));
	$$->name = $3;
	$$->next = $1;
}

| ALL
{
	$$ = NULL;
};

Tables: Name AS Name 
{
	$$ = (struct TableList *) malloc (sizeof (struct TableList));
	$$->tableName = $1;
	$$->aliasAs = $3;
	$$->next = NULL;
}

| Tables ',' Name AS Name
{
	$$ = (struct TableList *) malloc (sizeof (struct TableList));
	$$->tableName = $3;
	$$->aliasAs = $5;
	$$->next = $1;
};



CompoundExp: SimpleExp Op CompoundExp
{
	$$ = (struct FuncOperator *) malloc (sizeof (struct FuncOperator));	
	$$->leftOperator = (struct FuncOperator *) malloc (sizeof (struct FuncOperator));
	$$->leftOperator->leftOperator = NULL;
	$$->leftOperator->leftOperand = $1;
	$$->leftOperator->right = NULL;
	$$->leftOperand = NULL;
	$$->right = $3;
	$$->code = $2;	

}

| '(' CompoundExp ')' Op CompoundExp
{
	$$ = (struct FuncOperator *) malloc (sizeof (struct FuncOperator));	
	$$->leftOperator = $2;
	$$->leftOperand = NULL;
	$$->right = $5;
	$$->code = $4;	

}

| '(' CompoundExp ')'
{
	$$ = $2;

}

| SimpleExp
{
	$$ = (struct FuncOperator *) malloc (sizeof (struct FuncOperator));	
	$$->leftOperator = NULL;
	$$->leftOperand = $1;
	$$->right = NULL;	

}

| '-' CompoundExp
{
	$$ = (struct FuncOperator *) malloc (sizeof (struct FuncOperator));	
	$$->leftOperator = $2;
	$$->leftOperand = NULL;
	$$->right = NULL;	
	$$->code = '-';

}
;

Op: '-'
{
	$$ = '-';
}

| '+'
{
	$$ = '+';
}

| '*'
{
	$$ = '*';
}

| '/'
{
	$$ = '/';
}
;

AndList: '(' OrList ')' AND AndList
{
        // here we need to pre-pend the OrList to the AndList
        // first we allocate space for this node
        $$ = (struct AndList *) malloc (sizeof (struct AndList));
		final = $$;

        // hang the OrList off of the left
        $$->left = $2;

        // hang the AndList off of the right
        $$->rightAnd = $5;

}

| '(' OrList ')'
{
        // just return the OrList!
        $$ = (struct AndList *) malloc (sizeof (struct AndList));
		final = $$;
        $$->left = $2;
        $$->rightAnd = NULL;
}
;

OrList: Condition OR OrList
{
        // here we have to hang the condition off the left of the OrList
        $$ = (struct OrList *) malloc (sizeof (struct OrList));
        $$->left = $1;
        $$->rightOr = $3;
}

| Condition
{
        // nothing to hang off of the right
        $$ = (struct OrList *) malloc (sizeof (struct OrList));
        $$->left = $1;
        $$->rightOr = NULL;
}
;

Condition: Literal BoolComp Literal
{
        // in this case we have a simple literal/variable comparison
        $$ = $2;
        $$->left = $1;
        $$->right = $3;
}

| Literal
{
        $$ = (struct ComparisonOp *) malloc (sizeof (struct ComparisonOp));
        $$->code = EQUALS;
        $$->left = $1;
        $$->right = $1;
}
;

BoolComp: '<'
{
        // construct and send up the comparison
        $$ = (struct ComparisonOp *) malloc (sizeof (struct ComparisonOp));
        $$->code = LESS_THAN;
}

| '>'
{
        // construct and send up the comparison
        $$ = (struct ComparisonOp *) malloc (sizeof (struct ComparisonOp));
        $$->code = GREATER_THAN;
}

| '='
{
        // construct and send up the comparison
        $$ = (struct ComparisonOp *) malloc (sizeof (struct ComparisonOp));
        $$->code = EQUALS;
}
;

Literal : String
{
        // construct and send up the operand containing the string
        $$ = (struct Operand *) malloc (sizeof (struct Operand));
        $$->code = STR;
        $$->value = $1;
}

| Float
{
        // construct and send up the operand containing the FP number
        $$ = (struct Operand *) malloc (sizeof (struct Operand));
        $$->code = DBL;
        $$->value = $1;
}

| Int
{
        // construct and send up the operand containing the integer
        $$ = (struct Operand *) malloc (sizeof (struct Operand));
        $$->code = INT;
        $$->value = $1;
}

| Name
{
        // construct and send up the operand containing the name
        $$ = (struct Operand *) malloc (sizeof (struct Operand));
        $$->code = NAME;
        $$->value = $1;
}
;


SimpleExp: 

Float
{
        // construct and send up the operand containing the FP number
        $$ = (struct FuncOperand *) malloc (sizeof (struct FuncOperand));
        $$->code = DBL;
        $$->value = $1;
} 

| Int
{
        // construct and send up the operand containing the integer
        $$ = (struct FuncOperand *) malloc (sizeof (struct FuncOperand));
        $$->code = INT;
        $$->value = $1;
} 

| Name
{
        // construct and send up the operand containing the name
        $$ = (struct FuncOperand *) malloc (sizeof (struct FuncOperand));
        $$->code = NAME;
        $$->value = $1;
}
;

%%

