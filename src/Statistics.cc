#include <iostream>
#include <utility>
#include <fstream>
#include <cmath>
#include "Statistics.h"

Statistics::Statistics() {
	maxPartition = 0;
}

Statistics::Statistics(Statistics &copyMe) {
	maxPartition = copyMe.maxPartition;
	partition_map = copyMe.partition_map;
	stats = copyMe.stats;
}

Statistics::~Statistics() {

}

int Statistics::GetNumTuples(char *relName) {
	string relStr(relName);

	unordered_map<string,int>::iterator partItr = partition_map.find(relStr);
	if (partItr == partition_map.end()) {
		cout << "Error in " << __func__;
		cout << ": No relation with name - " << relName << endl;
		exit(0);
	}

	unordered_map<int,RelStats>::iterator relItr = stats.find(partItr->second);

	return relItr->second.numTuples;
}

void Statistics::AddRel(char *relName, int numTuples) {
	string relStr (relName);
	int partition;
	unordered_map<string,int>::iterator partItr;
	unordered_map<int,RelStats>::iterator relItr;

	partItr = partition_map.find(relStr);
	if (partItr == partition_map.end()) {
		partition = maxPartition+1;
		partition_map.insert(make_pair(relStr, partition));
		stats.insert(make_pair(partition, RelStats(numTuples, relStr)));
		maxPartition++;
	} else {
		relItr = stats.find(partItr->second);
		relItr->second.numTuples = numTuples;

		if (relItr->second.relNames.size() > 1) {
			for (auto& currRel: relItr->second.relNames)
				if (currRel != relStr)
					partition_map.erase(currRel);

			relItr->second.relNames.clear();
			relItr->second.attStats.clear();
			relItr->second.relNames.insert(relStr);
		}
	}
}

void Statistics::AddAtt(char *relName, char *attName, int numDistincts) {
	string relStr(relName);
	string attStr(attName);

	unordered_map<string,int>::iterator partItr = partition_map.find(relStr);
	if (partItr == partition_map.end()) {
		cout << "Error in AddAtt: No relation with name - " << relName << endl;
		exit(0);
	}

	unordered_map<int,RelStats>::iterator relItr = stats.find(partItr->second);

	if (numDistincts == -1)
		numDistincts = relItr->second.numTuples;

	unordered_map<string,int>::iterator attItr = relItr->second.attStats.find(attStr);
	if(attItr == relItr->second.attStats.end())
		relItr->second.attStats.insert(make_pair(attStr, numDistincts));
	else
		attItr->second = numDistincts;
}

void Statistics::CopyRel(char *oldName, char *newName) {
	string oldStr(oldName);
	string newStr(newName);
	int partition;

	unordered_map<string,int>::iterator partItr = partition_map.find(oldStr);
	if (partItr == partition_map.end()) {
		cout << "Error in " << __func__ << ": No relation with name - " << oldName << endl;
		exit(0);
	}

	unordered_map<int,RelStats>::iterator relItr = stats.find(partItr->second);

	RelStats newStats (relItr->second.numTuples, newStr);
	for (auto& attStat: relItr->second.attStats)
		newStats.attStats.insert(make_pair(newStr+"."+attStat.first, attStat.second));

	partItr = partition_map.find(newStr);
	if (partItr == partition_map.end()) {
		partition = maxPartition+1;
		partition_map.insert(make_pair(newStr, partition));
		stats.insert(make_pair(partition ,newStats));
		maxPartition++;
	} else {
		relItr = stats.find(partItr->second);
		if (relItr->second.relNames.size() > 1) {
			for (auto& currRel: relItr->second.relNames)
				if (currRel != newStr)
					partition_map.erase(currRel);
		}
		relItr->second = newStats;
	}
}
	
void Statistics::Read(char *fromWhere) {
	int numRels, numAtts, numStats, numPartitions;
	string relStr, attStr;
	int numTuples, numDistincts, partitionNum;

	partition_map.clear();
	stats.clear();

	ifstream inStream (fromWhere);
	if (!inStream.is_open())
		return;

	inStream >> maxPartition;
	inStream >> numPartitions;
	for (int i = 0; i < numPartitions; i++) {
		partitionNum;
		inStream >> relStr >> partitionNum;
		partition_map.insert(make_pair(relStr, partitionNum));
	}

	inStream >> numStats;
	for (int i = 0; i < numStats; i++) {
		RelStats newStats;

		inStream >> partitionNum >> newStats.numTuples;

		inStream >> numRels;
		for (int j = 0; j < numRels; j++) {
			inStream >> relStr;
			newStats.relNames.insert(relStr);
		}

		inStream >> numAtts;
		for (int j = 0; j < numAtts; j++) {
			inStream >> attStr >> numDistincts;
			newStats.attStats.insert(make_pair(attStr, numDistincts));
		}
		stats.insert(make_pair(partitionNum, newStats));
	}
	inStream.close();
}

void Statistics::Write(char *fromWhere) {
	ofstream outStream (fromWhere, ios::trunc);
	outStream << maxPartition << endl;
	outStream << partition_map.size() << endl;
	for (auto& part: partition_map)
		outStream << part.first << " " << part.second << endl;

	outStream << stats.size() << endl;
	for (auto& relStat: stats) {
		outStream << relStat.first << endl << relStat.second.numTuples << endl;

		// Writing relation names present in the partition
		outStream << relStat.second.relNames.size() << endl;
		for (auto& relName: relStat.second.relNames)
			outStream << relName << endl;

		outStream << relStat.second.attStats.size() << endl;
		for (auto& attStat: relStat.second.attStats)
			outStream << attStat.first << " " << attStat.second << endl;
	}
	outStream.close();
}

void Statistics::validateRequest(char **relNames, int numRels) {
	int count;
	string currRel;
	unordered_map<string,int>::iterator partItr;
	unordered_map<int,RelStats>::iterator relItr;
	unordered_map<string, bool> relMap;

	if (numRels == 0) {
		cout << "\nError: No relation names passed to join operation!\n" << endl;
		exit(0);
	}

	for (count = 0; count < numRels; count++) {
		currRel.assign(relNames[count]);
		if(!relMap.insert(make_pair(currRel, false)).second) {
			cout << "Error in join: " << currRel;
			cout << " is mentioned more than once in input relation names." << endl;
			exit(0);
		}
	}

	for (count = 0; count < numRels; count++) {
		currRel.assign(relNames[count]);
		if (relMap[currRel] == false) {
			partItr = partition_map.find(currRel);
			if (partItr == partition_map.end()) {
				cout << "\nError in join: No relation with name - " << currRel << "\n" << endl;
				exit(0);
			}

			relItr = stats.find(partItr->second);
			for (auto& relName: relItr->second.relNames) {
				if (relMap.find(relName) == relMap.end()) {
					cout << "\nError: Invalid join request. " << relName;
					cout << " was previously joined with one or more";
					cout << " of the relations mentioned in join.\n" << endl;
					exit(0);
				}
				relMap[relName] = true;
			}
		}
	}
}

unordered_map<string,int>::iterator Statistics::GetAttIterator(char **relNames,
	int numRels, string attName) {

	int count;
	string currRel;
	unordered_map<string,int>::iterator partItr;
	unordered_map<int,RelStats>::iterator relItr;
	unordered_map<string,int>::iterator attItr;

	for (count = 0; count < numRels; count++) {
		currRel.assign(relNames[count]);
		partItr = partition_map.find(currRel);
		relItr = stats.find(partItr->second);
		attItr = relItr->second.attStats.find(attName);
		if(attItr != relItr->second.attStats.end())
			break;
	}

	if (count == numRels) {
		cout << "\nError in " << __func__ << ": attribute " << attName;
		cout << " is not present in any of the relations mentioned in join\n" << endl;
		exit(0);
	}

	return attItr;
}

double Statistics::CrossProductVal(char **relNames,	int numRels) {
	int count;
	double result = 1.0;
	string currRel;
	unordered_map<string,int>::iterator partItr;
	unordered_map<int,RelStats>::iterator relItr;
	unordered_map<string, bool> relMap;

	for (count = 0; count < numRels; count++) {
		currRel.assign(relNames[count]);
		relMap.insert(make_pair(currRel, false));
	}

	for (count = 0; count < numRels; count++) {
		currRel.assign(relNames[count]);
		if (relMap[currRel] == false) {
			partItr = partition_map.find(currRel);
			relItr = stats.find(partItr->second);
			result *= (double)relItr->second.numTuples;
			for (auto& relName: relItr->second.relNames)
				relMap[relName] = true;
		}
	}

	return result;
}

void  Statistics::Apply(struct AndList *parseTree, char *relNames[], int numToJoin) {
	unordered_map<string,int>::iterator partItr;
	unordered_map<int,RelStats>::iterator mergeRelItr, currRelItr;
	int mergePartition, currPartition;
	double finalTuples = Estimate(parseTree, relNames, numToJoin);

	partItr = partition_map.find(relNames[0]);
	mergePartition = partItr->second;
	mergeRelItr = stats.find(partItr->second);

	for (int i = 0; i < numToJoin; i++) {
		partItr = partition_map.find(relNames[i]);
		if (partItr->second != mergePartition) {
			currPartition = partItr->second;
			currRelItr = stats.find(partItr->second);
			for (auto& relName: currRelItr->second.relNames) {
				partItr = partition_map.find(relName);
				partItr->second = mergePartition;
				mergeRelItr->second.relNames.insert(relName);
			}
			mergeRelItr->second.attStats.insert(currRelItr->second.attStats.begin(),
												currRelItr->second.attStats.end());
			stats.erase(currPartition);
		}
	}

	finalTuples += 0.5;
	mergeRelItr->second.numTuples = (int)finalTuples;
}

double Statistics::Estimate(struct AndList *parseTree, char **relNames, int numToJoin) {
	AndList *currAnd;
	OrList *currOr;
	double result = 0;
	double andSelectivity, currSelectivity;
	double totalSelectivity = 1;
	string currAtt, currRel;
	int count;
	unordered_map<string,int>::iterator leftAttItr, rightAttItr;

	validateRequest(relNames, numToJoin);

	result = CrossProductVal(relNames, numToJoin);

	currAnd = parseTree;
	while (currAnd != NULL) {
		string prevAtt;
		andSelectivity = 0;

		currOr = currAnd->left;
		while (currOr != NULL) {
			// currOr->left->left->code should be NAME
			if (currOr->left->left->code != NAME) {
				cout << "\nError in " << __func__ << ": Invalid query format!\n" << endl;
				exit(0);
			}

			currAtt.assign(currOr->left->left->value);
			leftAttItr = GetAttIterator(relNames, numToJoin, currAtt);

			if (currOr->left->right->code == NAME) {
				currAtt.assign(currOr->left->right->value);
				rightAttItr = GetAttIterator(relNames, numToJoin, currAtt);

				andSelectivity += (double)1/max(leftAttItr->second, rightAttItr->second);

			} else {

				if (currOr->left->code == EQUALS) {
					currSelectivity = (double)1/leftAttItr->second;
					if (currAtt == prevAtt)
						andSelectivity += currSelectivity;
					else
						andSelectivity = andSelectivity + currSelectivity - andSelectivity * currSelectivity;

				} else if (currOr->left->code == LESS_THAN || currOr->left->code == GREATER_THAN) {
					currSelectivity = (double)1/3;
					if (currAtt == prevAtt)
						andSelectivity += currSelectivity;
					else
						andSelectivity = andSelectivity + currSelectivity - andSelectivity * currSelectivity;

				} else {
					cout << "\nError in " << __func__ << ": unknown operator code " << currOr->left->code << endl;
					exit(0);
				}
				prevAtt = currAtt;
			}

			currOr = currOr->rightOr;
		}

		if (andSelectivity != 0)
			totalSelectivity *= andSelectivity;

		currAnd = currAnd->rightAnd;
	}

	result *= totalSelectivity;

	return result;
}

