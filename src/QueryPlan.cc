#include <iostream>
#include <cfloat>
#include <unordered_set>
#include "QueryPlan.h"

extern struct FuncOperator *finalFunction; // the aggregate function (NULL if no agg)
extern struct TableList *tables; // the list of tables and aliases in the query
extern struct AndList *boolean; // the predicate in the WHERE clause
extern struct NameList *groupingAtts; // grouping atts (NULL if no grouping)
extern struct NameList *attsToSelect; // the set of attributes in the SELECT (NULL if no such atts)
extern int distinctAtts; // 1 if there is a DISTINCT in a non-aggregate query 
extern int distinctFunc;  // 1 if there is a DISTINCT in an aggregate query

QueryPlan :: QueryPlan() {
	root = NULL;
	outFile = NULL;
	pipeCount = 0;
}

QueryPlan :: ~QueryPlan() {
	for (int i = 0; i < relAndList.size(); i++)
		DestroyAndList(relAndList[i]);

	for (int i = 0; i < relJoinAndList.size(); i++)
		DestroyAndList(relJoinAndList[i]);

	delete root;
}

AndList *QueryPlan :: CreateJoinAndList(AndList *leftAndList, Schema *leftSchema,
	AndList *rightAndList, Schema *rightSchema) {

	AndList *newAndList = NULL;
	OrList *newOrList = NULL;
	unordered_set<ComparisonOp *> condition_set;
	AndList *currAnd, *prevAnd, *nextAnd, *tempAnd;
	OrList *currOr, *prevOr, *nextOr, *tempOr;

	currAnd = leftAndList;
	while (currAnd != NULL) {
		tempAnd = new AndList();
		tempAnd->rightAnd = newAndList;
		newAndList = tempAnd;

		currOr = currAnd->left;
		newOrList = NULL;
		while (currOr != NULL) {
			tempOr = new OrList();
			tempOr->rightOr = newOrList;
			tempOr->left = currOr->left;
			newOrList = tempOr;

			currOr = currOr->rightOr;
		}
		newAndList->left = newOrList;
		currAnd = currAnd->rightAnd;
	}

	currAnd = rightAndList;
	while (currAnd != NULL) {
		tempAnd = new AndList();
		tempAnd->rightAnd = newAndList;
		newAndList = tempAnd;

		currOr = currAnd->left;
		newOrList = NULL;
		while (currOr != NULL) {
			tempOr = new OrList();
			tempOr->rightOr = newOrList;
			tempOr->left = currOr->left;
			newOrList = tempOr;

			currOr = currOr->rightOr;
		}
		newAndList->left = newOrList;
		currAnd = currAnd->rightAnd;
	}

	currAnd = newAndList;
	prevAnd = NULL;
	while (currAnd != NULL) {
		nextAnd = currAnd->rightAnd;

		currOr = currAnd->left;
		prevOr = NULL;
		while (currOr != NULL) {
			nextOr = currOr->rightOr;

			if (condition_set.find(currOr->left) != condition_set.end() ||
				(currOr->left->left->code == NAME && currOr->left->right->code == NAME &&
				 ((leftSchema->Find(currOr->left->left->value) == -1 ||
				   rightSchema->Find(currOr->left->right->value) == -1) &&
				  (leftSchema->Find(currOr->left->right->value) == -1 ||
				   rightSchema->Find(currOr->left->left->value) == -1)))) {
				delete currOr;
				if (prevOr == NULL)
					currAnd->left = nextOr;
				else
					prevOr->rightOr = nextOr;

			} else {
				condition_set.insert(currOr->left);
				prevOr = currOr;
			}
			currOr = nextOr;
		}
		if (prevOr == NULL) {
			delete currAnd;
			if (prevAnd == NULL)
				newAndList = nextAnd;
			else
				prevAnd->rightAnd = nextAnd;
		} else
			prevAnd = currAnd;

		currAnd = nextAnd;
	}

	return newAndList;
}

void QueryPlan :: DestroyAndList(AndList *tree) {
	AndList *currAnd, *nextAnd;
	OrList *currOr, *nextOr;

	currAnd = tree;
	while (currAnd != NULL) {
		nextAnd = currAnd->rightAnd;

		currOr = currAnd->left;
		while (currOr != NULL) {
			nextOr = currOr->rightOr;
			delete currOr;
			currOr = nextOr;
		}

		delete currAnd;
		currAnd = nextAnd;
	}
}

void QueryPlan :: CreateJoinNodes() {
	JoinNode *newNode;
	AndList *newAndList;
	vector<char *> joinRelNames;
	vector<bool> isJoined(relVector.size(), false);
	int left, right, NLPLeft, NLPRight;
	double minCost = DBL_MAX, NLPMinCost = DBL_MAX;

	// Join relations with minimum cost
	for (int i = 0; i < relVector.size(); i++) {
		for (int j = i+1; j < relVector.size(); j++) {
			joinRelNames.clear();
			joinRelNames.push_back(relNames.at(i));
			joinRelNames.push_back(relNames.at(j));

			newAndList = CreateJoinAndList(relJoinAndList[i],
				relVector[i]->outSchema, relJoinAndList[j],
				relVector[j]->outSchema);
			double currTuples = stats.Estimate(newAndList, joinRelNames.data(),
				joinRelNames.size());
			double currCost = currTuples + (double)stats.GetNumTuples(relNames.at(i)) +
				(double)stats.GetNumTuples(relNames.at(j));

			// Check if the join is going to be a Nested Loop join (NLP)
			if (newAndList != NULL) {
				if (currCost < minCost) {
					minCost = currCost;
					left = i;
					right = j;
				}
			} else {
				if (currCost < NLPMinCost) {
					NLPMinCost = currCost;
					NLPLeft = i;
					NLPRight = j;
				}
			}
			DestroyAndList(newAndList);
		}
	}
	if (minCost == DBL_MAX) {
		minCost = NLPMinCost;
		left = NLPLeft;
		right = NLPRight;
	}
	joinRelNames.clear();
	joinRelNames.push_back(relNames.at(left));
	joinRelNames.push_back(relNames.at(right));
	newAndList = CreateJoinAndList(relJoinAndList[left],
		relVector[left]->outSchema,	relJoinAndList[right], relVector[right]->outSchema);
	newNode = new JoinNode(relVector[left], relVector[right], newAndList);
	newNode->SetPipeID(pipeCount++);
	stats.Apply(newAndList, joinRelNames.data(), joinRelNames.size());
	isJoined.at(left) = true;
	isJoined.at(right) = true;
	DestroyAndList(newAndList);
	root = newNode;

	// Creating Left deep join plan
	for (int i = 0; i < relVector.size()-2; i++) {
		vector<char *> tempRelNames = joinRelNames;

		tempRelNames.push_back(relNames.at(0)); // temp
		minCost = NLPMinCost = DBL_MAX;
		for (int j = 0; j < relVector.size(); j++) {
			if (isJoined.at(j))
				continue;

			tempRelNames.back() = relNames.at(j);
			newAndList = CreateJoinAndList(NULL, root->outSchema,
				relJoinAndList[j], relVector[j]->outSchema);
			double currTuples = stats.Estimate(newAndList, tempRelNames.data(),
				tempRelNames.size());
			double currCost = currTuples + (double)stats.GetNumTuples(relNames.at(j));

			// Check if the join is going to be a Nested Loop join (NLP)
			if (newAndList != NULL) {
				if (currCost < minCost) {
					minCost = currCost;
					right = j;
				}
			} else {
				if (currCost < NLPMinCost) {
					NLPMinCost = currCost;
					NLPRight = j;
				}
			}
			DestroyAndList(newAndList);
		}
		if (minCost == DBL_MAX) {
			minCost = NLPMinCost;
			right = NLPRight;
		}
		joinRelNames.push_back(relNames.at(right));
		newAndList = CreateJoinAndList(NULL, root->outSchema,
			relJoinAndList[right], relVector[right]->outSchema);
		newNode = new JoinNode(root, relVector[right], newAndList);
		newNode->SetPipeID(pipeCount++);
		stats.Apply(newAndList, joinRelNames.data(), joinRelNames.size());
		isJoined.at(right) = true;

		DestroyAndList(newAndList);
		root = newNode;
	}

	//cout << "Num Tuples: " << numTuples << endl;
	//cout << "Join Tuples: " << joinTuples << endl;
	//cout << "Join nodes created" << endl;
}

void QueryPlan :: CreateSumNode() {
	SumNode *newNode = new SumNode(root, finalFunction);
	newNode->SetPipeID(pipeCount++);
	root = newNode;
	//cout << "Sum nodes created" << endl;
}

void QueryPlan :: CreateGroupByNode() {
	GroupByNode *newNode = new GroupByNode(root, groupingAtts, finalFunction);
	newNode->SetPipeID(pipeCount++);
	root = newNode;
	//cout << "Group By nodes created" << endl;
}

void QueryPlan :: CreateProjectNode(NameList *atts) {
	ProjectNode *newNode = new ProjectNode(root, atts);
	newNode->SetPipeID(pipeCount++);
	root = newNode;
	//cout << "Project nodes created" << endl;
}

void QueryPlan :: CreateDupRemovalNode() {
	DupRemovalNode *newNode = new DupRemovalNode(root);
	newNode->SetPipeID(pipeCount++);
	root = newNode;
	//cout << "Dup removal nodes created" << endl;
}

void QueryPlan :: CreateWriteOutNode() {
	WriteOutNode *newNode = new WriteOutNode(root, outFile);
	newNode->SetPipeID(pipeCount++);
	root = newNode;
	//cout << "WriteOut nodes created" << endl;
}

AndList *QueryPlan :: CreateRelAndList(AndList *parseTree, Schema *relSchema) {
	AndList *relAndList = NULL, *currAnd, *tempAndList;
	OrList *currOr, *relOr, *tempOrList;

	currAnd = parseTree;
	while (currAnd != NULL) {
		currOr = currAnd->left;
		relOr = NULL;
		while (currOr != NULL) {
			if (currOr->left->left->code == NAME &&
				currOr->left->right->code != NAME &&
				relSchema->Find(currOr->left->left->value) != -1) {

				if (!relOr) {
					tempAndList = new AndList();
					tempOrList = new OrList();
					tempOrList->rightOr = NULL;
					tempOrList->left = currOr->left;
					tempAndList->rightAnd = relAndList;
					tempAndList->left = tempOrList;
					relAndList = tempAndList;
					relOr = tempOrList;
				} else {
					tempOrList = new OrList();
					tempOrList->rightOr = NULL;
					tempOrList->left = currOr->left;
					relOr->rightOr = tempOrList;
					relOr = tempOrList;
				}
			}
			currOr = currOr->rightOr;
		}
		currAnd = currAnd->rightAnd;
	}

	return relAndList;
}

// Specific to a particular relation
AndList *QueryPlan :: CreateRelJoinAndList(AndList *parseTree, Schema *relSchema) {
	AndList *relAndList = NULL, *currAnd, *tempAndList;
	OrList *currOr, *relOr, *tempOrList;

	currAnd = parseTree;
	while (currAnd != NULL) {
		currOr = currAnd->left;
		relOr = NULL;
		while (currOr != NULL) {
			if (currOr->left->left->code == NAME &&
				currOr->left->right->code == NAME && 
				(relSchema->Find(currOr->left->left->value) != -1 ||
				 relSchema->Find(currOr->left->right->value) != -1)) {

				if (!relOr) {
					tempAndList = new AndList();
					tempOrList = new OrList();
					tempOrList->rightOr = NULL;
					tempOrList->left = currOr->left;
					tempAndList->rightAnd = relAndList;
					tempAndList->left = tempOrList;
					relAndList = tempAndList;
					relOr = tempOrList;
				} else {
					tempOrList = new OrList();
					tempOrList->rightOr = NULL;
					tempOrList->left = currOr->left;
					relOr->rightOr = tempOrList;
					relOr = tempOrList;
				}
			}
			currOr = currOr->rightOr;
		}
		currAnd = currAnd->rightAnd;
	}

	return relAndList;
}

void QueryPlan :: Init() {
	TableList *currTable;
	RelNode *newNode;
	AndList *newAndList;

	stats.Read("Statistics.txt");

	// Create alias relations in Statistics object
	// and create respective rel nodes to build the query tree
	currTable = tables;
	while (currTable != NULL) {
		stats.CopyRel(currTable->tableName, currTable->aliasAs);
		newNode = new RelNode(currTable->tableName, currTable->aliasAs);
		newNode->SetPipeID(pipeCount++);
		relVector.push_back(newNode);
		relNames.push_back(currTable->aliasAs);
		newAndList = CreateRelAndList(boolean, newNode->outSchema);
		relAndList.push_back(newAndList);
		relJoinAndList.push_back(CreateRelJoinAndList(boolean, newNode->outSchema));
		newNode->CreateCNF(newAndList);
		currTable = currTable->next;
	}

	// Checks for any issues in the parseTree
	int numTuples = stats.Estimate(boolean, relNames.data(), relNames.size());

	isQueryPlanReady = false;
}

void QueryPlan :: Build() {
	if (relVector.size() == 0) {
		cerr << "Error in " << __func__ << ": Query plan is not initialized!" << endl;
		exit(0);
	}

	root = relVector.at(0);

	for (int i = 0; i < relVector.size(); i++)
		stats.Apply(relAndList[i], &relNames[i], 1);

	if (relVector.size() > 1)
		CreateJoinNodes();

	// GROUP BY / SUM
	if (groupingAtts != NULL && distinctFunc == 0) {
		CreateGroupByNode();
		if (attsToSelect != NULL && finalFunction != NULL) {
			NameList *prevAtt, *newAtt, *currAtt = attsToSelect;
			while(currAtt != NULL) {
				prevAtt = currAtt;
				currAtt = currAtt->next;
			}
			newAtt = new NameList();
			newAtt->name = "sum";
			newAtt->next = NULL;
			prevAtt->next = newAtt;
		}

	} else if (finalFunction != NULL) {
		// Distinct SUM
		if (distinctFunc == 1) {
			CreateProjectNode(groupingAtts);
			CreateDupRemovalNode();
		}
		CreateSumNode();
	}

	// Project
	if (attsToSelect != NULL) {
		CreateProjectNode(attsToSelect);

		// Duplicate removal
		if (distinctAtts == 1)
			CreateDupRemovalNode();
	}

	CreateWriteOutNode();

	isQueryPlanReady = true;
}

void QueryPlan :: Run(int output, char *outputFile) {

	Init();

	if (output == FILE_OUTPUT) {
		cout << "Results will be written to " << outputFile << endl;
		outFile = fopen(outputFile, "w");
	}

	Build();

	if (output == NO_OUTPUT) {
		Print();
		return;
	}

	if (!isQueryPlanReady) {
		cerr << "Error in " << __func__ << ": Query plan is not built yet!" << endl;
		exit(0);
	}

	// parseTree is NULL if vector.size() > 1
	for (int i = 0; i < relVector.size(); i++)
		relVector.at(i)->Run();

	root->WaitUntilDone();
}

void QueryPlan :: Print() {
	cout << "\nOptimized query plan for the given SQL query:\n" << endl;
	root->Print();
}
