#include <iostream>
#include <cstdlib>
#include "GenericDBFile.h"
#include "Defs.h"

GenericDBFile :: GenericDBFile () {
	dbPageIndex = 0;
	opMode = None;
	sortInfo = NULL;
	dbPath = NULL;
	isNewCnf = true;
}

GenericDBFile :: ~GenericDBFile () {
}

int GenericDBFile :: Open (int len, char *f_path, SortInfo *sortInfo) {
	this->sortInfo = sortInfo;
	dbPath = f_path;

	diskFile.Open(len, f_path);

	return 1;
}

int GenericDBFile :: Close () {
	FlushToDisk();
	dbPageIndex = 0;
	opMode = None;
	sortInfo = NULL;
	dbPath = NULL;
	return diskFile.Close();
}

void GenericDBFile :: Load (Schema &f_schema, char *loadpath) {
	Record dbRecord;
	FILE *tableFile = fopen (loadpath, "r");

	while (dbRecord.SuckNextRecord (&f_schema, tableFile) == 1)
		Add(dbRecord);
}

void GenericDBFile :: MoveFirst () {
	if (opMode == Read)
		pageBuffer.EmptyItOut();

	dbPageIndex = 0;
	isNewCnf = true;
}

int GenericDBFile :: GetNext (Record &fetchme) {
	int result;

	// If prevOperation is write then we have a dirty page in memory.
	// Write this dirty page to disk before we read-in.
	if (opMode == Write)
		FlushToDisk();

	result = pageBuffer.GetFirst(&fetchme);

	// No more records in in-memory page? Read next page from disk.
	if (dbPageIndex < (diskFile.GetLength()-1) && result == 0) {
		diskFile.GetPage(&pageBuffer, dbPageIndex);
		dbPageIndex++;
		result = pageBuffer.GetFirst(&fetchme);
	}

	opMode = Read;

	return result;
}
