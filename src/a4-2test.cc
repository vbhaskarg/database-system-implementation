#include "QueryPlan.h"

int main () {
	QueryPlan qPlan;

	qPlan.Init();
	qPlan.Build();
	qPlan.Print();
	qPlan.Run();

	return 0;
}
