#ifndef DBFILE_H
#define DBFILE_H

#include "GenericDBFile.h"

using namespace std;

typedef enum {heap, sorted, tree} fType;

class DBFile {
private:
	GenericDBFile *internalDBFile;
	char db_metadata_path[100];
	fType db_type;
	SortInfo sortInfo;
	
	void DBInit (fType f_type);

public:
	DBFile (); 
    ~DBFile ();

	int Create (char *fpath, fType file_type, void *startup);
	int Open (char *fpath);
	int Close ();

	void Load (Schema &myschema, char *loadpath);
	void MoveFirst ();
	void Add (Record &addme);
	int GetNext (Record &fetchme);
	int GetNext (Record &fetchme, CNF &cnf, Record &literal);

};
#endif
