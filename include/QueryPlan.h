#ifndef QUERYPLAN_H
#define QUERYPLAN_H

#include <vector>
#include "Statistics.h"
#include "ParseTree.h"
#include "Node.h"

class QueryPlan {
	Node *root;
	bool isQueryPlanReady;
	Statistics stats;
	vector<RelNode *> relVector;
	vector<char *> relNames;
	vector<AndList *> relAndList;
	vector<AndList *> relJoinAndList;
	FILE *outFile;
	int pipeCount;


	void Init();

	void Build();

	void Print();

	void CreateJoinNodes();
	void CreateSumNode();
	void CreateGroupByNode();
	void CreateProjectNode(NameList *atts);
	void CreateDupRemovalNode();
	void CreateWriteOutNode();
	AndList *CreateRelAndList(AndList *parseTree, Schema *relSchema);
	AndList *CreateRelJoinAndList(AndList *parseTree, Schema *relSchema);
	AndList *CreateJoinAndList(AndList *leftAndList, Schema *leftSchema,
		AndList *rightAndList, Schema *rightSchema);
	void DestroyAndList(AndList *tree);

public:
	QueryPlan();
	~QueryPlan();

	void Run(int output, char *outputFile);
};

#endif
