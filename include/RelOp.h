#ifndef REL_OP_H
#define REL_OP_H

#include "Pipe.h"
#include "DBFile.h"
#include "Record.h"
#include "Function.h"

class RelationalOp {
	protected:
	pthread_t opThread;
	int runlen;

	public:
	// blocks the caller until the particular relational operator 
	// has run to completion
	virtual void WaitUntilDone ();

	// tell us how much internal memory the operation can use
	void Use_n_Pages (int n);

	virtual void RunThread() =0;
	static void *RunThreadHelper(void *context);
};

class SelectFile : public RelationalOp {
	private:
	DBFile *inFilePtr;
	Pipe *outPipePtr;
	CNF *selOpPtr;
	Record *literalPtr;

	public:
	void Run (DBFile &inFile, Pipe &outPipe, CNF &selOp, Record &literal);
	void RunThread ();
};

class SelectPipe : public RelationalOp {
	private:
	Pipe *inPipePtr, *outPipePtr;
	CNF *selOpPtr;
	Record *literalPtr;

	public:
	void Run (Pipe &inPipe, Pipe &outPipe, CNF &selOp, Record &literal);
	void RunThread ();
};

class Project : public RelationalOp {
	private:
	Pipe *inPipePtr, *outPipePtr;
	int *attsArray;
	int numAttsInput;
	int numAttsOutput;

	public:
	void Run (Pipe &inPipe, Pipe &outPipe, int *keepMe, int numAttsInput, int numAttsOutput);
	void RunThread ();
};

class Join : public RelationalOp {
	private:
	Pipe *leftPipePtr, *rightPipePtr, *outPipePtr;
	CNF *selOpPtr;
	Record *literalPtr;

	DBFile rightRecDB;
	int leftRecCount, rightRecCount;
	bool isLeftEmpty, isRightEmpty;
	int maxRecs;
	Record *leftRecBuff;
	Record *rightRecBuff;
	int numAttsLeft, numAttsRight, numAttsToKeep, startOfRight;
	int *attsToKeep;
	ComparisonEngine comp;

	void ComputeMergeParams (Record &leftRec, Record &rightRec);
	void JoinBuffRecs ();
	void JoinWithRightRecs ();
	void SortMergeJoin (OrderMaker &leftOrder, OrderMaker &rightOrder);
	void NestedLoopJoin ();

	public:
	void Run (Pipe &inPipeL, Pipe &inPipeR, Pipe &outPipe, CNF &selOp, Record &literal);
	void RunThread ();
};

class DuplicateRemoval : public RelationalOp {
	private:
	Pipe *inPipePtr, *outPipePtr;
	Schema *schemaPtr;

	public:
	void Run (Pipe &inPipe, Pipe &outPipe, Schema &mySchema);
	void RunThread ();
};

class Sum : public RelationalOp {
	private:
	Pipe *inPipePtr, *outPipePtr;
	Function *functionPtr;

	public:
	void Run (Pipe &inPipe, Pipe &outPipe, Function &computeMe);
	void RunThread ();
};

class GroupBy : public RelationalOp {
	private:
	Pipe *inPipePtr, *outPipePtr;
	Pipe *sumIn, *sumOut;
	OrderMaker *myOrderPtr;
	Function *functionPtr;
	Record currRec, prevRec, sumRec, outRec;
	ComparisonEngine comp;
	Sum sum;

	void StartNewGroupSum ();
	void WritePrevGroupSum (int numAttsLeft, int numAttsRight,
		int *attsToKeep, int numAttsToKeep, int startOfRight);

	public:
	void Run (Pipe &inPipe, Pipe &outPipe, OrderMaker &groupAtts, Function &computeMe);
	void RunThread ();
};

class WriteOut : public RelationalOp {
	private:
	Pipe *inPipePtr;
	FILE *outFilePtr;
	Schema *schemaPtr;

	public:
	void Run (Pipe &inPipe, FILE *outFile, Schema &mySchema);
	void RunThread ();
};
#endif
