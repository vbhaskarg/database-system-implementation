#ifndef NODE_H
#define NODE_H

#include <iostream>
#include "Schema.h"
#include "Pipe.h"
#include "RelOp.h"
#include "Record.h"
#include "Comparison.h"
#include "ParseTree.h"

using namespace std;

#define PIPE_SIZE 100
#define NUM_PAGES 100

class Node {
public:
	Schema *outSchema;
	Pipe *outPipe;
	int outPipeId;
	Node *outNode;

	Node();
	virtual ~Node();

	virtual void Print() {}
	virtual void WaitUntilDone () {}

	void SetPipeID(int pipeId) {
		outPipeId = pipeId;
	}
};

class RelNode : public Node {
	SelectFile relSelect;
	CNF cnf;
	Record literal;
	char *tableName, *alias;
	DBFile dbFile;

public:
	RelNode(char *tableName, char *alias);
	~RelNode();

	void CreateCNF(AndList *relAndList);
	void Run();
	void Print();
};

class JoinNode : public Node {
	Pipe *leftInPipe, *rightInPipe;
	Node *leftInNode, *rightInNode;
	CNF cnf;
	Record literal;
	Join relJoin;

public:
	JoinNode(Node *leftInNode, Node *rightInNode, AndList *parseTree);
	~JoinNode();

	void Print();
};


class SumNode : public Node {
	Pipe *inPipe;
	Node *inNode;
	Function func;
	Sum sum;

public:
	SumNode(Node *inNode, FuncOperator *finalFunction);
	~SumNode();

	void Print();
};

class GroupByNode : public Node {
	Pipe *inPipe;
	Node *inNode;
	Function func;
	OrderMaker *order;
	GroupBy groupby;

public:
	GroupByNode(Node *inNode, NameList *groupingAtts, FuncOperator *finalFunction);
	~GroupByNode();

	void Print();
};

class ProjectNode : public Node {
	Pipe *inPipe;
	Node *inNode;
	Schema *inSchema;
	Project project;
	vector<int> attVector;
	vector<Attribute> atts;

public:
	ProjectNode(Node *inNode, NameList *attsList);
	~ProjectNode();

	void Print();
};

class DupRemovalNode : public Node {
	Pipe *inPipe;
	Node *inNode;
	DuplicateRemoval dupRemoval;

public:
	DupRemovalNode(Node *inNode);
	~DupRemovalNode();

	void Print();
};

class WriteOutNode : public Node {
	Pipe *inPipe;
	Node *inNode;
	WriteOut out;

public:
	WriteOutNode(Node *inNode, FILE *outFile);
	~WriteOutNode();

	void WaitUntilDone ();
	void Print();
};

#endif
