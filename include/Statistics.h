#ifndef STATISTICS_
#define STATISTICS_

#include <unordered_map>
#include <unordered_set>
#include "ParseTree.h"

using namespace std;

struct RelStats {
	int numTuples;
	unordered_set<string> relNames;
	unordered_map<string,int> attStats;

	RelStats() : numTuples(0) {}
	RelStats(int n, string relName) : numTuples(n) {relNames.insert(relName);}
};

class Statistics
{
	int maxPartition;
	unordered_map<string,int> partition_map;
	unordered_map<int,RelStats> stats;

public:
	Statistics();
	Statistics(Statistics &copyMe);	 // Performs deep copy
	~Statistics();

	int GetNumTuples(char *relName);
	void AddRel(char *relName, int numTuples);
	void AddAtt(char *relName, char *attName, int numDistincts);
	void CopyRel(char *oldName, char *newName);

	void Read(char *fromWhere);
	void Write(char *fromWhere);

	void validateRequest(char **relNames, int numToJoin);
	double CrossProductVal(char **relNames,	int numRels);
	unordered_map<string,int>::iterator GetAttIterator(char **relNames,
		int numRels, string attName);

	void  Apply(struct AndList *parseTree, char *relNames[], int numToJoin);
	double Estimate(struct AndList *parseTree, char **relNames, int numToJoin);

};

#endif
