#ifndef GENERICDBFILE_H
#define GENERICDBFILE_H

#include "TwoWayList.h"
#include "Record.h"
#include "Schema.h"
#include "File.h"
#include "Comparison.h"
#include "ComparisonEngine.h"
#include "BigQ.h"

using namespace std;

typedef enum {None, Read, Write} dbOperation;

struct SortInfo {
	OrderMaker *myOrder;
	int runLength;
};

struct QueryInfo {
	OrderMaker queryOrder;
	int attPosition[MAX_ANDS];
};

class GenericDBFile {
protected:
	int dbPageIndex;
	File diskFile;
	Page pageBuffer;
	dbOperation opMode;
	SortInfo *sortInfo;
	char *dbPath;
	bool isNewCnf;

	virtual void FlushToDisk () =0;

public:
	GenericDBFile (); 
	virtual ~GenericDBFile ();

	int Open (int len, char *fpath, SortInfo *sortInfo);
	int Close ();

	void Load (Schema &myschema, char *loadpath);
	void MoveFirst ();
	virtual void Add (Record &addme) =0;
	int GetNext (Record &fetchme);
	virtual int GetNext (Record &fetchme, CNF &cnf, Record &literal) =0;

};

class HeapDBFile: public GenericDBFile {
private:
	void FlushToDisk ();

public:
	HeapDBFile (); 
	~HeapDBFile ();
	
	void Add (Record &addme);
	int GetNext (Record &fetchme, CNF &cnf, Record &literal);

};

class SortedDBFile: public GenericDBFile {
private:
	// pipe cache size
	int buffsz;
	Pipe *inPipe;
	Pipe *outPipe;
	BigQ *bigQ;
	QueryInfo qInfo;

	void SetupBigQ ();
	void FlushToDisk ();
	void BuildQueryOrder (CNF &cnf);
	int GetFirstRecord(int pageIndex, Record &fetchme);
	int GetMatchingRec(int index, Record &fetchme, CNF &cnf, Record &literal);
	int BinarySearch(Record &fetchme, CNF &cnf, Record &literal);

	// returns a -1, 0, or 1 depending upon whether given record is less then,
	// equal to, or greater than literal record, depending upon the OrderMaker
	int CompareLiteral(Record *record, Record *literal);

public:
	SortedDBFile (); 
	~SortedDBFile ();

	void Add (Record &addme);
	int GetNext (Record &fetchme, CNF &cnf, Record &literal);

};
#endif
