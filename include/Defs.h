#ifndef DEFS_H
#define DEFS_H

#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;

#define MAX_ANDS 20
#define MAX_ORS 20

#define PAGE_SIZE 131072

#define SETTINGS_PATH "../src/test.cat"
#define db_catalog_path "DB_Catalog"
#define catalog_path "catalog"
#define tpch_dir "/cise/tmp/dbi_sp11/DATA/1G/"

enum Target {Left, Right, Literal};
enum CompOperator {LessThan, GreaterThan, Equals};
enum Type {Int, Double, String};


unsigned int Random_Generate();


inline string GetTypeStr (Type type) {
	string str;

	switch (type) {
		case Int:
			str.assign("Int");
			break;
		case Double:
			str.assign("Double");
			break;
		case String:
			str.assign("String");
			break;
		default:
			str.assign("Unknown");
	}

	return str;
}

inline void GetRandomStr(char *str, int len) {
	string alpha =
		"abcdefghijklmnopqrstuvwxyz";

	for (int i = 0; i < len; ++i)
		str[i] = alpha.at(rand() % alpha.size());

	str[len] = '\0';
}

#endif

