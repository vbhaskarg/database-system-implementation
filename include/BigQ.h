#ifndef BIGQ_H
#define BIGQ_H
#include <pthread.h>
#include <iostream>
#include <vector>
#include "Pipe.h"
#include "File.h"
#include "Record.h"

using namespace std;

struct RunStatus {
	int currLen;
	Page currPage;
};

class BigQ {
private:

	int nOfRuns;
	int runlen;
	Pipe *inPipeRef;
	Pipe *outPipeRef;
	OrderMaker *sortorderRef;
	vector<Record *> bigQVec;
	RunStatus *runArray;
	Record **runHeadPtr;
	Page bigQPage;
	File bigQFile;

	bool AddToVector();

	void flushToDisk();

	void sortAndWriteRun();

	void MergeSetup();

	// Gets the mininum record from the run heads.
	bool GetNextRecord(Record *recPtr);

	// Generates sorted runs and writes to disk
	void GenerateRuns();

	// Gets sorted runs from disk and writes to out pipe
	// in sorted order
	void MergeRuns();

	void *SortRecords();

	static void *SortRecords_helper(void *context);

public:

	BigQ (Pipe &in, Pipe &out, OrderMaker &sortorder, int runlen);
	~BigQ ();

};

#endif
