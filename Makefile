CC = g++-4.8 -std=c++11 -O2 -Wno-deprecated

tag = -i

ifdef linux
tag = -n
endif

SRC_DIR = ./src
DATA_DIR = ./data
BIN_DIR = ./bin
OBJ_DIR = ./obj
INCLUDE_DIR = ./include

$(shell   mkdir -p $(OBJ_DIR))

$(shell   mkdir -p $(BIN_DIR))

$(shell   cp $(SRC_DIR)/catalog $(BIN_DIR)/catalog)

$(shell   cp $(SRC_DIR)/Statistics.txt $(BIN_DIR)/Statistics.txt)

all: a1test a2-1test a2-2test a3test a4-1test main

main: Statistics.o \
	Record.o \
	Comparison.o \
    ComparisonEngine.o \
    Schema.o \
    Pipe.o \
    RelOp.o \
    Function.o \
    File.o \
    BigQ.o \
    DBFile.o \
    GenericDBFile.o \
    HeapDBFile.o \
    SortedDBFile.o \
	Node.o \
    QueryPlan.o \
    y.tab.o \
    lex.yy.o \
    main.o
	$(CC) -o $(BIN_DIR)/main $(OBJ_DIR)/Statistics.o \
	    $(OBJ_DIR)/Record.o \
	    $(OBJ_DIR)/Comparison.o \
	    $(OBJ_DIR)/ComparisonEngine.o \
	    $(OBJ_DIR)/Schema.o \
	    $(OBJ_DIR)/Pipe.o \
	    $(OBJ_DIR)/RelOp.o \
	    $(OBJ_DIR)/Function.o \
	    $(OBJ_DIR)/File.o \
	    $(OBJ_DIR)/BigQ.o \
	    $(OBJ_DIR)/DBFile.o \
	    $(OBJ_DIR)/GenericDBFile.o \
	    $(OBJ_DIR)/HeapDBFile.o \
	    $(OBJ_DIR)/SortedDBFile.o \
	    $(OBJ_DIR)/Node.o \
	    $(OBJ_DIR)/QueryPlan.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/main.o -lfl -lpthread

a4-1test: Statistics.o \
    y.tab.o \
    lex.yy.o \
    a4-1test.o
	$(CC) -o $(BIN_DIR)/a4-1test $(OBJ_DIR)/Statistics.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/a4-1test.o -lfl

a3test: Record.o \
    Comparison.o \
    ComparisonEngine.o \
    Schema.o \
    File.o \
    BigQ.o \
    DBFile.o \
    GenericDBFile.o \
    HeapDBFile.o \
    SortedDBFile.o \
    Pipe.o \
    RelOp.o \
    Function.o \
    y.tab.o \
    yyfunc.tab.o \
    lex.yy.o \
    lex.yyfunc.o \
    a3test.o
	$(CC) -o $(BIN_DIR)/a3test $(OBJ_DIR)/Record.o \
	    $(OBJ_DIR)/Comparison.o \
	    $(OBJ_DIR)/ComparisonEngine.o \
	    $(OBJ_DIR)/Schema.o \
	    $(OBJ_DIR)/File.o \
	    $(OBJ_DIR)/BigQ.o \
	    $(OBJ_DIR)/DBFile.o \
	    $(OBJ_DIR)/GenericDBFile.o \
	    $(OBJ_DIR)/HeapDBFile.o \
	    $(OBJ_DIR)/SortedDBFile.o \
	    $(OBJ_DIR)/Pipe.o \
	    $(OBJ_DIR)/RelOp.o \
	    $(OBJ_DIR)/Function.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/yyfunc.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/lex.yyfunc.o \
	    $(OBJ_DIR)/a3test.o -lfl -lpthread

a2-2test: Record.o \
    Comparison.o \
    ComparisonEngine.o \
    Schema.o \
    File.o \
    BigQ.o \
    DBFile.o \
    GenericDBFile.o \
    HeapDBFile.o \
    SortedDBFile.o \
    Pipe.o \
    y.tab.o \
    lex.yy.o \
    a2-2test.o
	$(CC) -o $(BIN_DIR)/a2-2test $(OBJ_DIR)/Record.o \
	    $(OBJ_DIR)/Comparison.o \
	    $(OBJ_DIR)/ComparisonEngine.o \
	    $(OBJ_DIR)/Schema.o \
	    $(OBJ_DIR)/File.o \
	    $(OBJ_DIR)/BigQ.o \
	    $(OBJ_DIR)/DBFile.o \
	    $(OBJ_DIR)/GenericDBFile.o \
	    $(OBJ_DIR)/HeapDBFile.o \
	    $(OBJ_DIR)/SortedDBFile.o \
	    $(OBJ_DIR)/Pipe.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/a2-2test.o -lfl -lpthread

a2-1test: Record.o \
    Comparison.o \
    ComparisonEngine.o \
    Schema.o \
    File.o \
    BigQ.o \
    DBFile.o \
    GenericDBFile.o \
    HeapDBFile.o \
    SortedDBFile.o \
    Pipe.o \
    y.tab.o \
    lex.yy.o \
    a2test.o
	$(CC) -o $(BIN_DIR)/a2-1test $(OBJ_DIR)/Record.o \
	    $(OBJ_DIR)/Comparison.o \
	    $(OBJ_DIR)/ComparisonEngine.o \
	    $(OBJ_DIR)/Schema.o \
	    $(OBJ_DIR)/File.o \
	    $(OBJ_DIR)/BigQ.o \
	    $(OBJ_DIR)/DBFile.o \
	    $(OBJ_DIR)/GenericDBFile.o \
	    $(OBJ_DIR)/HeapDBFile.o \
	    $(OBJ_DIR)/SortedDBFile.o \
	    $(OBJ_DIR)/Pipe.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/a2test.o -lfl -lpthread

a1test: Record.o \
    Comparison.o \
    ComparisonEngine.o \
    Schema.o \
    File.o \
    BigQ.o \
    DBFile.o \
    GenericDBFile.o \
    HeapDBFile.o \
    SortedDBFile.o \
    Pipe.o \
    y.tab.o \
    lex.yy.o \
    a1test.o
	$(CC) -o $(BIN_DIR)/a1test $(OBJ_DIR)/Record.o \
	    $(OBJ_DIR)/Comparison.o \
	    $(OBJ_DIR)/ComparisonEngine.o \
	    $(OBJ_DIR)/Schema.o \
	    $(OBJ_DIR)/File.o \
	    $(OBJ_DIR)/BigQ.o \
	    $(OBJ_DIR)/DBFile.o \
	    $(OBJ_DIR)/GenericDBFile.o \
	    $(OBJ_DIR)/HeapDBFile.o \
	    $(OBJ_DIR)/SortedDBFile.o \
	    $(OBJ_DIR)/Pipe.o \
	    $(OBJ_DIR)/y.tab.o \
	    $(OBJ_DIR)/lex.yy.o \
	    $(OBJ_DIR)/a1test.o -lfl -lpthread

main.o: $(SRC_DIR)/main.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/main.o $(SRC_DIR)/main.cc

a4-1test.o: $(SRC_DIR)/a4-1test.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/a4-1test.o $(SRC_DIR)/a4-1test.cc

a3test.o: $(SRC_DIR)/a3test.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/a3test.o $(SRC_DIR)/a3test.cc

a2-2test.o: $(SRC_DIR)/a2-2test.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/a2-2test.o $(SRC_DIR)/a2-2test.cc

a2test.o: $(SRC_DIR)/a2test.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/a2test.o $(SRC_DIR)/a2test.cc

a1test.o: $(SRC_DIR)/a1test.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/a1test.o $(SRC_DIR)/a1test.cc

Comparison.o: $(SRC_DIR)/Comparison.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Comparison.o $(SRC_DIR)/Comparison.cc
	
ComparisonEngine.o: $(SRC_DIR)/ComparisonEngine.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/ComparisonEngine.o $(SRC_DIR)/ComparisonEngine.cc

Pipe.o: $(SRC_DIR)/Pipe.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Pipe.o $(SRC_DIR)/Pipe.cc

BigQ.o: $(SRC_DIR)/BigQ.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/BigQ.o $(SRC_DIR)/BigQ.cc

RelOp.o: $(SRC_DIR)/RelOp.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/RelOp.o $(SRC_DIR)/RelOp.cc

Function.o: $(SRC_DIR)/Function.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Function.o $(SRC_DIR)/Function.cc

DBFile.o: $(SRC_DIR)/DBFile.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/DBFile.o $(SRC_DIR)/DBFile.cc

GenericDBFile.o: $(SRC_DIR)/GenericDBFile.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/GenericDBFile.o $(SRC_DIR)/GenericDBFile.cc

HeapDBFile.o: $(SRC_DIR)/HeapDBFile.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/HeapDBFile.o $(SRC_DIR)/HeapDBFile.cc

SortedDBFile.o: $(SRC_DIR)/SortedDBFile.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/SortedDBFile.o $(SRC_DIR)/SortedDBFile.cc

Statistics.o: $(SRC_DIR)/Statistics.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Statistics.o $(SRC_DIR)/Statistics.cc

QueryPlan.o: $(SRC_DIR)/QueryPlan.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/QueryPlan.o $(SRC_DIR)/QueryPlan.cc

Node.o: $(SRC_DIR)/Node.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Node.o $(SRC_DIR)/Node.cc

File.o: $(SRC_DIR)/File.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/File.o $(SRC_DIR)/File.cc

Record.o: $(SRC_DIR)/Record.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Record.o $(SRC_DIR)/Record.cc

Schema.o: $(SRC_DIR)/Schema.cc
	$(CC) -g -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/Schema.o $(SRC_DIR)/Schema.cc

y.tab.o: $(SRC_DIR)/Parser.y
	yacc -d -o $(OBJ_DIR)/y.tab.c $(SRC_DIR)/Parser.y
	sed $(tag) $(OBJ_DIR)/y.tab.c -e "s/  __attribute__ ((__unused__))$$/# ifndef __cplusplus\n  __attribute__ ((__unused__));\n# endif/"
	g++ -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/y.tab.o $(OBJ_DIR)/y.tab.c

yyfunc.tab.o: $(SRC_DIR)/ParserFunc.y
	yacc -p "yyfunc" -b "yyfunc" -d -o $(OBJ_DIR)/yyfunc.tab.c $(SRC_DIR)/ParserFunc.y
	#sed $(tag) $(OBJ_DIR)/yyfunc.tab.c -e "s/  __attribute__ ((__unused__))$$/# ifndef __cplusplus\n  __attribute__ ((__unused__));\n# endif/" 
	g++ -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/yyfunc.tab.o $(OBJ_DIR)/yyfunc.tab.c

lex.yy.o: $(SRC_DIR)/Lexer.l
	lex -o $(OBJ_DIR)/lex.yy.c $(SRC_DIR)/Lexer.l
	gcc -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/lex.yy.o $(OBJ_DIR)/lex.yy.c

lex.yyfunc.o: $(SRC_DIR)/LexerFunc.l
	lex -Pyyfunc -o $(OBJ_DIR)/lex.yyfunc.c $(SRC_DIR)/LexerFunc.l
	gcc  -c -I$(INCLUDE_DIR) -o $(OBJ_DIR)/lex.yyfunc.o $(OBJ_DIR)/lex.yyfunc.c

clean: 
	rm -f $(OBJ_DIR)/* 
	rm -f $(BIN_DIR)/*

